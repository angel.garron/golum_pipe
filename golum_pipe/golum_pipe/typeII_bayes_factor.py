"""
Script to start the computation of the type II image
Bayes factor
"""
import re
import sys
from golum_pipe.postprocessing.bayes_factor_type_II import main

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(main())