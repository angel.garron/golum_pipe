"""
Script computing the Bayes factor for type II images
once the run is done
"""
import os
from golum.tools.postprocessing import compute_log_bayes_factors_type_II_vs_type_I_and_vs_type_III
from bilby_pipe.utils import parse_args, get_command_line_arguments
from golum_pipe.generic_plot import create_parser
from golum_pipe.core.utils import golum_logger
import json
from bilby.gw.result import CBCResult

def compute_and_save_type_II_bayes_factors():
    """
    Function computing the type II image Bayes factors
    using the dedicated GOLUM function and saving it
    in the result out directory
    """

    args, unknown_args = parse_args(get_command_line_arguments(), create_parser())
    result_file = args.result
    logB_IIvsI, logB_IIvsIII = compute_log_bayes_factors_type_II_vs_type_I_and_vs_type_III(file = result_file)

    # make the outdir where the data will be save
    if hasattr(args, 'webdir'):
        outdir = os.path.join(args.webdir, 'bayes_factor_type_II')
    elif hasattr(args, 'outdir'):
        outdir = args.outdir
    else:
        outdir = args.outdir
    golum_logger.info(f"Type II image Bayes factors will be output in {outdir}")
    
    extension = os.path.splitext(args.result)[-1][1:]
    if extension == "json":
       result = CBCResult.from_json(args.result)
    elif extension == "hdf5":
        result = CBCResult.from_hdf5(args.result)
    elif extension == "pkl":
        result = CBCResult.from_pickle(args.result)
    else:
        raise ValueError(f"Result format {extension} not understood.")

    

    with open(f'{outdir}/{result.label}_type_II_bayes_factors.json', 'w') as f:
        json.dump({'logB_IIvsI' : logB_IIvsI, 
                   'logB_IIvsIII' : logB_IIvsIII}, f,
                   indent = 4)


def main():
    """
    Top level interface for the computation of the 
    type II image Bayes factor
    """

    compute_and_save_type_II_bayes_factors()
