"""
Script actually performing the computation of the 
coherence ratio for pair when done with the usual 
golum runs
"""
import os
import sys
import bilby 
import json
from bilby_pipe.bilbyargparser import BilbyArgParser
from bilby_pipe.utils import parse_args
from golum_pipe.core.utils import golum_logger_img2
from golum.tools import conversion as golumconversion

def standard_prior_setter():
    """
    Function setting up a standard prior to which the
    evidence is recomputed when computing the coherence ratio.
    This makes for comparable results. Bounds should be big
    enough to encompass all the events


    RETURNS:
    --------
    - std_prior_dict: a standard prior Bilby prior dictionary
    """

    std_prior_dict = bilby.gw.prior.BBHPriorDict()
    std_prior_dict.pop('mass_1')
    std_prior_dict.pop('mass_2')
    std_prior_dict['chirp_mass'] = bilby.core.prior.Uniform(name = 'chirp_mass', latex_label = '$M_{c}$',
                                                            minimum = 2., maximum = 300.,
                                                            unit = '$M_{\\odot}$')

    std_prior_dict['mass_ratio'] = bilby.core.prior.Uniform(name = 'mass_ratio', latex_label = '$q$',
                                                            minimum = 0.05, maximum = 1.)
    std_prior_dict['mass_1'] = bilby.core.prior.Constraint(name = 'mass_1', latex_label = '$M_1$',
                                                           minimum = 1., maximum = 1000.)
    std_prior_dict['mass_2'] = bilby.core.prior.Constraint(name = 'mass_2', latex_label = '$M_1$',
                                                           minimum = 1., maximum = 1000.)
    std_prior_dict['luminosity_distance'] = bilby.gw.prior.UniformComovingVolume(name = 'luminosity_distance',
                                                                                 latex_label = '$D_l$',
                                                                                 minimum = 100,
                                                                                 maximum = 5e5)
    return std_prior_dict


def create_clu_parser():
    """
    Create a parser adapted for the coherence ratio 
    computation
    """
    parser = BilbyArgParser(ignore_unknown_config_file_keys = True)
    parser.add("--result", type=str, required = True, help="The result file")
    parser.add("--unlensed-file-image-1", type = str, default = 'None', help = 'Path to the unlensed result file for the first image')
    parser.add("--unlensed-file-image-2", type = str, default = 'None', help = 'Path to the unlensed result file for the second image')
    parser.add("--posterior-file-image-1", type = str, default = 'None', help = 'Path to the lensed result for the first image')

    return parser

def compute_usual_golum_coherence_ratio(args, unknown_args):
    """
    Function actually doing the job, computing
    the coherence ratio and writing it into the result file

    ARGUMENTS:
    ----------
    - args: the arguments known to the parser
    - unknown_args: the arguments not known to the sampler
    """

    # setup the output directory
    if hasattr(args, "webdir"):
        outdir = os.path.join(args.webdir, "bilby")
    elif hasattr(args, "outdir"):
        outdir = args.outdir
    else:
        outdir = None

    img2_unl_file = args.unlensed_file_image_2
    if img2_unl_file == 'None':
        raise ValueError(f"The unlensed image 2 file {img2_unl_file} is not found")

    # read in the results
    img2_lens_results = bilby.result.read_in_result(filename = args.result)
    if outdir is None:
        outdir = img2_lens_results.outdir

    std_prior = standard_prior_setter()
    logZ_img2_unl, _ = golumconversion.change_in_prior(img2_unl_file, std_prior)

    if args.unlensed_file_image_1 == 'None' or args.posterior_file_image_1 == 'None':
        golum_logger_img2.info('The useful information for the first image was not provided, computing the information for second image only')

        log_clu = img2_lens_results.log_evidence - logZ_img2_unl

    else:
        golum_logger_img2.info('Computing the coherence ratio using the two images')

        logZ_img1_unl, _ = golumconversion.change_in_prior(args.unlensed_file_image_1, std_prior)
        logZ_img1_lens, _ = golumconversion.change_in_prior(args.posterior_file_image_1, std_prior)

        log_clu = img2_lens_results.log_evidence + logZ_img1_lens - logZ_img1_unl - logZ_img2_unl

    # output the result
    outfile = f'{outdir}/{img2_lens_results.label}_log_coherence_ratio.json'
    golum_logger_img2.info(f"Saving the log coherence ratio in {outfile}")
    with open(outfile, 'w') as fout:
        json.dump({'log_clu' : float(log_clu)}, fout,
                  indent = 4)

def main():
    """
    Top level interface to compute coherence ratio
    """
    args, unknown_args = parse_args(sys.argv[1:], create_clu_parser())
    compute_usual_golum_coherence_ratio(args, unknown_args)
