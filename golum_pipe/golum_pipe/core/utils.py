"""
Utility to setup the GOLUM runs using pipe
"""

import os 
import ast
from configparser import ConfigParser
import logging
import bilby
import golum 
import json
import bilby_pipe
from golum.tools import utils as golumutils
from importlib import import_module
import smtplib

def verify_config_file(args):
    """
    Function verifying whether the ini file exists and
    reading it. Raises errors in case the file is non existing
    or not readable

    ARGUMENTS:
    ----------
    - args: command line arguments given by the user

    RETURNS:
    --------
    - config: the INI configuration parser
    """

    config = ConfigParser(converters = {'dict' : lambda x: ast.literal_eval(x)})

    if not os.path.isfile(args.ini):
        raise IOError(f"{args.ini} does not exist")

    # since the file exists, try to read it
    try:
        config.read(args.ini)
    except IOError:
        print(f"Cannot read file {args.ini}")

    return config

def parse_ini_file(ini_file):
    """
    Helper function converting an ini file in an a configparser

    ARGUMENTS:
    ----------
    - ini_file: the ini file to read

    RETURNS:
    --------
    - config: the config object
    """

    config = ConfigParser(converters = {'dict' : lambda x: ast.literal_eval(x)})

    if not os.path.isfile(ini_file):
        raise IOError(f"{ini_file} does not exist")

    # since the file exists, try to read it
    try:
        config.read(ini_file)
    except IOError:
        print(f"Cannot read file {ini_file}")

    return config



def make_waveform_arguments(config, n_img = None, injection = False):
    """
    Reads in the information from the config and uses it
    to setup the waveform argument dictionary to be
    used by bilby. We use the usual bilby WaveformGenerator.
    So, no changes required.

    ARGUMENTS:
    ----------
    - config: the arguments coming from the config file
    - n_img: the image number. If None, then no arguments is added
             Else, int whether we look at image 1 or 2 
             in a symmetric or a joint run. 

    RETURNS:
    --------
    - waveform_arguments: dictionary to be used to set the
                          run up
    """

    waveform_arguments = {}

    # list of parameters to look for
    params = ['waveform-approximant', 'reference-frequency',
              'minimum-frequency', 'maximum-frequency']

    if n_img is None:
        if injection:
            for par in params:
                val = config.get('injection_settings', f'injection-{par}', fallback = None)
                if val is None:
                    val = config.get('analysis_settings', f'{par}', fallback = None)

                waveform_arguments[par] = val
        else:
            for par in params:
                val = config.get('analysis_settings', f'{par}', fallback = None)
                waveform_arguments[par] = val
    else:
        if injection:
            for par in params:
                val = config.get('injection_settings_image_%i'%n_img, f'injection-{par}', fallback = None)
                if val is None:
                    val = config.get('analysis_settings', f'{par}', fallback = None)

                waveform_arguments[par] = val
        else:
            for par in params:
                val = config.get('analysis_settings', f'{par}', fallback = None)
                waveform_arguments[par] = val

    return waveform_arguments

def make_injection_file(config, img2 = False, symmetric = None):
    """
    Function reading information from the configuration 
    to setup the injection file using `create_injection_file`
    from bilby_pipe

    ARGUMENTS:
    ----------
    - config: the configurations read from the ini file
    - img2: whether we are in the img2 case or not
    - Symmetric: default is None. If not None, then 
                 used to have an adapted injection file. 
                 Else, make it img1img2 or img2img1

    RETURNS:
    --------
    - injection_file: the file containing the injection file
                      information made by bilby_pipe
    """
    outd = config.get('output_settings', 'outdir')
    if symmetric is None:
        injection_file = f'{outd}/data/injection.dat'
    else:
        injection_file = f'{outd}/data/injection_{symmetric}.dat'

    # verify in the config whether we are using a 
    # prior file or a prior dict
    # if we are in the second image case, also see if we 
    # can set the injection up from the first image
    trigg_time = None
    if img2 is True and config.getboolean('injection_settings', 'injection-from-first-image', fallback = False):
        golum_logger_img2.info("Trying to set the injection from first image file and lensing parameters")
        img1_file = config.get('golum_settings', 'posterior-file-image-1')
        if os.path.isfile(img1_file):
            # read in the file using the GOLUM way
            img1_parameters, _ = golumutils.read_image1_file(file = img1_file)
            if img1_parameters is not None:
                
                try:
                    lensing_parameters = config.getdict('injection_settings', 'lensing-parameters-dict')
                    inj_dict = golumutils.make_bbh_parameters_from_first_image_parameters_and_lensing_parameters(
                                           img1_parameters, lensing_parameters)
                    trigg_time = inj_dict['geocent_time']
                    # convert the in another file
                    prior_file = injection_file.replace('injection.dat', 'prior.dat')
                    with open(prior_file, 'w', encoding='utf-8') as prior:
                        for key, value in inj_dict.items():
                            prior.write(f'{key} = {value} \n')

                except:
                    raise IOError("Issue when setting up the injection from the first image result file")

            else:
                raise IOError("The provided image 1 result file does not contain the injection parameters")

        else:
            raise IOError(f"{img1_file} is not an existing file")

    elif config.get('injection_settings', 'injection-dict') is None and config.get('injection_settings', 'injection-file') is not None:
        # parse the injection file 
        inj_file = inj_file
    elif config.getdict('injection_settings', 'injection-dict') is not None:
        # use the injection dictionary 
        #inj_dict = json.loads(config.get('injection_settings', 'injection-dict'))
        inj_dict = config.getdict('injection_settings', 'injection-dict')
        # make prior file
        prior_file = injection_file.replace('injection.dat', 'prior.dat')

        with open(prior_file, 'w', encoding='utf-8') as prior:
            for key, value in inj_dict.items():
                prior.write(f'{key} = {value} \n')
    else:
        raise ValueError('You did not specify the injection parameters')

    # read GPS time info if needed
    gps_tuple = config.get('analysis_settings', 'gps-tuple', fallback = None)
    gps_file = config.get('analysis_settings', 'gps-file', fallback = None)

    if gps_tuple is not None:
        gps_times = bilby_pipe.input.Input.parse_gps_tuple(gps_tuple)
    elif gps_file is not None:
        gps_times = bilby_pipe.input.Input.parse_gps_tuple(gps_file)
    else:
        gps_times = None

    # setup the other arguments required for the run 
    n_injection = config.getint('injection_settings', 'n-simulation', fallback = 1)
    if trigg_time is None:
        trigger_time = config.getfloat('analysis_settings', 'trigger-time', fallback = 0)
    else:
        trigger_time = trigg_time
    delta_t = config.getfloat('analysis_settings', 'deltaT', fallback = 0.2)
    duration = config.getfloat('analysis_settings', 'duration', fallback = 4)
    post_trigger_duration = config.getfloat('analysis_settings', 'post-trigger-duration', fallback = 2.)
    generation_seed = config.getint('injection_settings', 'generation-seed', fallback = None)
    enforce_signal_duration = config.getboolean('injection_settings', 'enforce-signal-duration', fallback = False)

    # use bilby to make the injection file
    bilby_pipe.create_injections.create_injection_file(injection_file,
                                                       prior_file = prior_file,
                                                       prior_dict = None,
                                                       n_injection = n_injection,
                                                       trigger_time = trigger_time,
                                                       deltaT = delta_t,
                                                       gpstimes = gps_times,
                                                       duration = duration, 
                                                       post_trigger_duration = post_trigger_duration,
                                                       generation_seed = generation_seed,
                                                       enforce_signal_duration = enforce_signal_duration)

    return injection_file

def setup_logger(outdir = None, log_level = 'INFO', args = None):
    """
    Function setting up a logger for the golum job

    ARGUMENTS:
    ----------
    - outdir: directory containing the run output
    - log_level: the level at which the logger should print info
    - args: command line arguments 
    """

    # if one want a verbose run, put the most possible text
    if args is not  None:
        if args.verbose: 
            log_level = "DEBUG"

    # see if the requested level exists in the logging 
    if isinstance(log_level, str):
        try:
            log_level = getattr(logging, log_level.upper())
        except AttributeError as exc:
            raise ValueError(f"{log_level.upper()} is not a valid logging level")

    # setup the logger 
    golum_logger = logging.getLogger('Golum')
    golum_logger.handlers.clear()
    golum_logger.propagate = False
    logging.basicConfig(level = log_level)

    console_handler = logging.StreamHandler()
    console_handler.setLevel(log_level)
    golum_logger.addHandler(console_handler)

    # also make a log file and complete it if outdir is specified
    if outdir is not None:
        # run the bilby command to see if the file exists 
        # and make it if it is not the case
        bilby.core.utils.check_directory_exists_and_if_not_mkdir(outdir)

        # setup the log file 
        logfile = f"{outdir}/golum.log"
        file_handler = logging.FileHandler(logfile)
        file_handler.setLevel(log_level)

        formatter = logging.Formatter("%(asctime)s %(name)s %(levelname)s: %(message)s")
        formatter.datefmt = "%H:%M:%S %d/%m/%Y"
        console_handler.setFormatter(formatter)
        file_handler.setFormatter(formatter)

        golum_logger.addHandler(file_handler)

    golum_logger.debug("Logger has been setup")

setup_logger()
golum_logger = logging.getLogger(' golum_image1 ')
golum_logger_img2 = logging.getLogger(' golum_image2 ')
symmetric_golum_logger = logging.getLogger(' symmetric_golum ')
joint_pe_golum_logger = logging.getLogger(' joint_pe_golum ')

def make_final_dag(config, ouput_directories, img):
    """
    Function to make the final dag using the bilby_pipe
    dag file and other functions

    ARGUMENTS:
    ----------
    - config: the ini configuration file
    - output_directories: dictionary of the output directories
    - img: whether we are running for image 1 or 2
    RETURNS:
    --------
    - final_dag: the final dag file containing the linked jobs 
    """

    label = config.get('output_settings', 'label')
    subdir = ouput_directories['submit']
    final_dag_file = f'{subdir}/golum_image{img}_{label}.dag'
    bilby_pipe_dag_file = f'{subdir}/dag_{label}.submit'

    with open(final_dag_file, 'w', encoding  = 'utf-8') as dag:
        dag.write(f'SUBDAG EXTERNAL bilby_pipe_dag {bilby_pipe_dag_file} \n')

    golum_logger.info('Dag file generated')

    return final_dag_file

def make_final_dag_joint(config, ouput_directories):
    """
    Function to make the final dag using the bilby_pipe
    dag file and other functions for the joint PE case

    ARGUMENTS:
    ----------
    - config: the ini configuration file
    - output_directories: dictionary of the output directories
    RETURNS:
    --------
    - final_dag: the final dag file containing the linked jobs 
    """

    label = config.get('output_settings', 'label')
    subdir = ouput_directories['submit']
    final_dag_file = f'{subdir}/joint_pe_golum_{label}.dag'
    bilby_pipe_dag_file = f'{subdir}/dag_{label}.submit'

    with open(final_dag_file, 'w', encoding  = 'utf-8') as dag:
        dag.write(f'SUBDAG EXTERNAL bilby_pipe_dag {bilby_pipe_dag_file} \n')

    golum_logger.info('Dag file generated')

    return final_dag_file


def make_final_dag_symmetric(config, output_directories):
    """
    Function to make the final dag for the symmetric run
    """

    label = config.get('output_settings', 'label')
    subdir = ouput_directories['submit']
    final_dag_file = f'{subdir}/golum_symmetric_{label}.dag'
    bilby_pipe_dag_file = f'{subdir}/dag_{label}.submit'

    with open(final_dag_file, 'w', encoding  = 'utf-8') as dag:
        dag.write(f'SUBDAG EXTERNAL bilby_pipe_dag {bilby_pipe_dag_file} \n')

    symmetric_golum_logger.info('Dag file generated')

    return final_dag_file



def get_function_from_string(python_path):
    """
    Fuction translating the python path argument to
    the actual function we want

    ARGUMENTS:
    ----------
    - python_path: the full python name to the function

    RETURNS:
    --------
    - fct: the prior function as a bilby or GOLUM prior
    """

    function = python_path.split('(', 1)
    function[-1] = '('+function[-1]
    if function[0] in ['Uniform', 'DeltaFunction', 'Sine', 'Gaussian', 'PowerLaw', 'LogUniform', 
                       'SymmetricLogUniform', 'Gaussian', 'Normal', 'TruncatedGaussian', 'TruncatedNormal', 
                       'HalfGaussian', 'HalfNormal', 'LogNormal', 'LogGaussian', 'Exponential', 
                       'StudentT', 'Beta', 'Logistic', 'Cauchy', 'Lorentzian' 'Gamma',
                       'ChiSqared', 'FermiDirac', 'Categorical']:
        module = 'bilby.core.prior'
    else:
        module_list = function[0].split('.')[:-1]
        module = ".".join(module_list)

    func_str = function[0].split('.')[-1]
    try:
        mod_fct = getattr(import_module(module), func_str)
    except ImportError as e:
        raise ImportError(f"Failed to load function {python_path}, full message: {e}")

    # set the different arguments 
    function_elements = {}
    fs = function[-1].split(',')
    fs[0] = fs[0].replace('(', '')
    fs[1] = fs[1].replace(')', '')
    for el in fs:
        function_elements[el.split('=')[0]] = el.split('=')[1].replace('"\'', '"').replace('\'"', '"')

    if 'unit' not in function_elements.keys():
        function_elements['unit'] = None
    if 'boundary' not in function_elements.keys():
        function_elements['boundary'] = None
    if 'latex_label' not in function_elements.keys():
        function_elements['latex_label'] = None
    if 'name' not in function_elements.keys():
        function_elements['name'] = None

    if func_str not in ['DeltaFunction', 'PowerLaw', 'Sine', 'Cosine'] and module == 'bilby.core.prior':
        fct = mod_fct(name = function_elements['name'], minimum = float(function_elements['minimum']),
                      maximum = float(function_elements['maximum']), latex_label = function_elements['latex_label'],
                      unit = function_elements['unit'], boundary = function_elements['boundary'])
    elif func_str in ['Sine', 'Cosine'] and module == 'bilby.core.prior':
        fct = mod_fct(name = function_elements['name'], latex_label = function_elements['latex_label'],
                      unit = function_elements['unit'], boundary = function_elements['boundary'])
    elif func_str == 'DeltaFunction' and module == 'bilby.core.prior':
        fct = mod_fct(peak = float(function_elements['peak']), name = function_elements['name'],
                      latex_label = function_elements['latex_label'], unit = function_elements['unit'])
    elif func_str == 'PowerLaw' and module == 'bilby.core.prior':
        fct = mod_fct(alpha = float(function_elements['alpha']), minimum = float(function_elements['minimum']),
                      maximum = float(function_elements['maximum']), name = function_elements['name'],
                      latex_label = function_elements['latex_label'], unit = function_elements['unit'],
                      boundary = function_elements['boundary'])
    elif func_str == 'MorseFactorDifference':
        fct = mod_fct(name = function_elements['name'], latex_label = function_elements['latex_label'])
    elif func_str == 'MorseFactorPrior':
        fct = mod_fct(name = function_elements['name'], latex_label = function_elements['latex_label'])
    else:
        raise ValueError(f'Prior {module}.{func_str} is not understood')

    return fct


def read_prior(prior, dictionary = False, file = False):
    """
    Function reading in the prior argument passed to the
    ini and returning a prior dictionary. 

    ARGUMENTS:
    ----------
    - prior: the prior information to be passed
    - dictionary: whether we are passing the arguments coming 
                  from a prior dictionary
    - file: whether we are passing the prior file as an argument

    RETURNS:
    --------
    - pdict: a dictionary with the priors which can be used
             by GOLUM
    """

    if dictionary == file:
        raise ValueError('You did not specify the prior type correctly')
    elif dictionary is True and file is False:
        # reading information from a dictionary passed to the
        # ini file
        # need to convert it to an actual string dictionary
        t = prior.replace('{', '').replace('}', '')
        if '), ' in prior:
            tt = t.split('), ')
        elif '),' in prior:
            tt = t.split('),')
        elif ') ,' in prior:
            tt = t.split(') ,')
        for i in range(len(tt) -1):
            tt[i].append(')')

        pdict = {}
        for el in tt:
            ell = el.split(':')
            pdict[ell[0].replace('\'', '').replace(' ', '')] = ell[1].replace('\'', '').replace(' ', '')

        for key in pdict:
            pdict[key] = get_function_from_string(pdict[key])

        return pdict


    elif file is True and dictionary is False:
        # reading information from a file passed to the ini
        if not os.path.isfile(prior):
            raise IOError(f'The prior file {prior} does not exist')
        pdict = {}

        with open(prior, 'r') as f:
            lines = f.readlines()

        for l in lines:
            pdict[l.split('=', 1)[0].replace(' ', '')] = l.split('=', 1)[1].replace(' ', '').replace('\n', '')

        # convert to actual functions
        for key in pdict:
            pdict[key] = get_function_from_string(pdict[key])

        return pdict


def make_symm_injection_file(config, n_img):
    """
    Function to make the injection file when we want
    to inject two different images

    ARGUMENTS:
    ----------
    - config: the configuration read from the ini file
    - n_img: int, whether we want to get the injection for the
             first or the second image


    RETURNS:
    --------
    - injection_file: the file containing the injection 
                      information to pass to bilby 
    """

    outd = config.get('output_settings', 'outdir')
    injection_file = f'{outd}/dat/injection_image{n_img}.dat'

    # check in the config file whether the injection should
    # be read from file or not
    if n_img == 1:
        if config.getboolean('injection_settings_image_1', 'injection-from-second-image', fallback = False):
            img2_file = config.get('golum_settings', 'posterior-file-image-2')

            if os.path.isfile(img2_file):
                # read in the information 
                img2_parameters, _ = golumutils.read_image1_file(file = img2_file)

                if img2_parameters is not None:
                    try:
                        lensing_parameters = config.getdict('injection_settings_image_1', 'lensing-parameters-dict')
                        inj_dict = golumutils.make_bbh_parameters_from_first_image_parameters_and_lensing_parameters(
                                            img2_parameters, lensing_parameters)
                        trigg_time = inj_dict['geocent_time']
                        # write the file
                        prior_file = injection_file.replace(f'injection_image{n_img}.dat', f'prior_image{n_img}.dat')
                        with open(prior_file, 'w', encoding='utf-8') as prior:
                            for key, value in inj_dict.items():
                                prior.write(f'{key} = {value} \n')
                    except:
                        raise IOError("Issue when setting up the injection from the second result file")

                else:
                    raise IOError("The provided image 2 file does not contain injection information")

            else:
                raise IOError(f'{img2_file} is not an existing file')
    if n_img == 2:
        if config.getboolean('injection_settings_image_2', 'injection-from-first-image', fallback = False):
            img1_file = config.get('golum_settings', 'posterior-file-image-1')

            if os.path.isfile(img1_file):
                # read in the information 
                img1_parameters, _ = golumutils.read_image1_file(file = img1_file)

                if img1_parameters is not None:
                    try:
                        lensing_parameters = config.getdict('injection_settings_image_2', 'lensing-parameters-dict')
                        inj_dict = golumutils.make_bbh_parameters_from_first_image_parameters_and_lensing_parameters(
                                            img1_parameters, lensing_parameters)
                        trigg_time = inj_dict['geocent_time']
                        # write the file
                        prior_file = injection_file.replace(f'injection_image{n_img}.dat', f'prior_image{n_img}.dat')
                        with open(prior_file, 'w', encoding='utf-8') as prior:
                            for key, value in inj_dict.items():
                                prior.write(f'{key} = {value} \n')
                    except:
                        raise IOError("Issue when setting up the injection from the second result file")

                else:
                    raise IOError("The provided image 2 file does not contain injection information")

            else:
                raise IOError(f'{img1_file} is not an existing file')


    # we do not consider the gps_tuple and file options here
    gps_times = None

    # setup the arguments required for the run 
    n_injection = config.getint('injection_settings_image_%i'%n_img, 'n-simulation')
    if trigg_time is None:
        trigger_time = config.getfloat('analysis_settings', 'trigger-time-image-%i'%n_img, fallback = 0)
    else:
        trigger_time = trigg_time

    delta_t = config.getfloat('analysis_settings', 'deltaT', fallback = 0.2)
    duration = config.getfloat('analysis_settings', 'duration', fallback = 4)
    post_trigger_duration = config.getfloat('analysis_settings', 'post-trigger-duration', fallback = 2.)
    generation_seed = config.getfloat('injection_settings_image_%i'%n_img, 'generation-seed', fallback = None)
    enforce_signal_duration = config.getboolean('injection_settings_image_%i'%n_img, 'enforce-signal-duration', fallback = False)


    # use bilby pipe to make the injection file for the image
    bilby_pipe.create_injections.create_injection_file(injection_file,
                                                       prior_file = prior_file,
                                                       prior_dict = None,
                                                       n_injection = n_injection,
                                                       trigger_time = trigger_time,
                                                       deltaT = delta_t,
                                                       gpstimes = gps_times,
                                                       duration = duration, 
                                                       post_trigger_duration = post_trigger_duration,
                                                       generation_seed = generation_seed,
                                                       enforce_signal_duration = enforce_signal_duration)

    return injection_file

def make_golum_config_from_symmetric(symmetric_config, n_img):
    """
    A function to generate the config for one of the GOLUM runs
    when submitting the ini file for the symmetric Golum run.

    ARGUMENTS:
    ---------
    - symmetric_config: the output of the symmetric ini run
    - n_img: whether the first image is image 1 or image 2
    """
    # setup the complementary image number
    if n_img == 1:
        n_comp = 2
    elif n_img == 2:
        n_comp = 1
    else:
        raise ValueError("The symmetric run cannot be run for more than 2 images")

    # setup an empty parse
    golum_config = ConfigParser(converters = {'dict' : lambda x: ast.literal_eval(x)})

    # for the output section, keep the same, except the label
    golum_config.add_section('output_settings')
    for key, value in symmetric_config.items('output_settings'):
        if key == 'label':
            
            new_label = value+"img%iimg%i"%(n_img, n_comp)
            golum_config.set('output_settings', 'label', new_label)

        else:
            golum_config.set('output_settings', key, value)

    # set the analysis settings
    golum_config.add_section('analysis_settings')
    golum_config.add_section('golum_settings')
    for key, value in symmetric_config.items('analysis_settings'):
        if 'trigger-time-' in key:
            if key == 'trigger-time-image-%i'%n_comp:
                golum_config.set('analysis_settings', 'trigger-time', value)
            elif key == 'trigger-time-image-%i'%n_img:
                golum_config.set('golum_settings', 'trigger-time-image-1', value)
        else:
            golum_config.set('analysis_settings', key, value)

    # set the sampler settings
    golum_config.add_section('sampler_settings')
    for key, value in symmetric_config.items('sampler_settings'):
        golum_config.set('sampler_settings', key, value)

    # set the golum settings
    for key, value in symmetric_config.items('golum_settings'):
        if 'posterior-file-image' in key:
            if key == 'posterior-file-image-1' and n_img == 1:
                golum_config.set('golum_settings', 'posterior-file-image-1', value)
            elif key == 'posterior-file-image-2' and n_img == 2:
                golum_config.set('golum_settings', 'posterior-file-image-1', value)
        elif key == 'unlensed-file-image-1':
            if n_img == 1:
                golum_config.set('golum_settings', 'unlensed-file-image-1', value)
            else:
                golum_config.set('golum_settings', 'unlensed-file-image-2', value)
        elif key == 'unlensed-file-image-2':
            if n_img == 2:
                golum_config.set('golum_settings', 'unlensed-file-image-1', value)
            else:
                golum_config.set('golum_settings', 'unlensed-file-image-2', value)
        elif key=='compute-coherence-ratio':
            golum_config.set('golum_settings', 'compute-coherence-ratio', 'False')
        elif key == 'joint-posterior-reweighting':
            golum_config.set('golum_settings', 'joint-posterior-reweighting', 'False')
        else:
            golum_config.set('golum_settings', key, value)

    # set the prior settings
    golum_config.add_section('prior_settings')
    for key, value in symmetric_config.items('prior_settings'):
        golum_config.set('prior_settings', key, value)

    # set the event settings
    golum_config.add_section('event_settings')
    for key, value in symmetric_config.items('event_%i_settings'%n_comp):
        golum_config.set('event_settings', key, value)

    # set the injection settings 
    golum_config.add_section('injection_settings')
    for key, value in symmetric_config.items('injection_settings_image_%i'%n_comp):
        if 'injection-from' in key:
            golum_config.set('injection_settings', 'injection-from-first-image', value)
        else:
            golum_config.set('injection_settings', key, value)

    # set the condor settings
    golum_config.add_section('condor_settings')
    for key, value in symmetric_config.items('condor_settings'):
        if key == 'submit':
            golum_config.set('condor_settings', 'submit', 'False')
        else:
            golum_config.set('condor_settings', key, value)

    # set the other settings
    golum_config.add_section('other_bilby_settings')
    for key, value in symmetric_config.items('other_bilby_settings'):
        golum_config.set('other_bilby_settings', key, value)

    return golum_config

def make_golum_config_from_joint_pe(config, n_img):
    """
    Make the golum config for the individual images based on the 
    config file for the joint run
    """

    # make an empty parser 
    golum_config = ConfigParser(converters = {'dict' : lambda x : ast.literal_eval(x)})

    if n_img == 1:
        n_comp = 2
    elif n_img == 2:
        n_comp = 1

    # output is changed as to have another label
    golum_config.add_section('output_settings')
    for key, value in config.items('output_settings'):
        for key, value in config.items('output_settings'):
            if key == 'label':
            
                new_label = value+"image%i"%(n_img)
                golum_config.set('output_settings', 'label', new_label)

            else:
                golum_config.set('output_settings', key, value)

    # analysis settings
    golum_config.add_section('analysis_settings')
    for key, value in config.items('analysis_settings'):
        if 'trigger-time-' in key:
            if key == 'trigger-time-image-%i'%n_img:
                golum_config.set('analysis_settings', 'trigger-time', value)
        if 'detectors-image-' in key:
            if key == 'detectors-image-%i'%n_img:
                golum_config.set('analysis_settings', 'detectors', value)
        else:
            golum_config.set('analysis_settings', key, value)

    # set the sampler settings
    golum_config.add_section('sampler_settings')
    for key, value in config.items('sampler_settings'):
        golum_config.set('sampler_settings', key, value)

    # set the golum settings
    golum_config.add_section('golum_settings')
    for key, value in config.items('golum_settings'):
        if 'unlensed-file-' in key:
            if key == 'unlensed-file-image-1' and n_img == 1:
                golum_config.set('golum_settings', 'unlensed-file-image-1', value)
                golum_config.set('golum_settings', 'unlensed-file-image-2', value)
            elif key == 'unlensed-file-image-2' and n_img == 2:
                golum_config.set('golum_settings', 'unlensed-file-image-2', value)
                golum_config.set('golum_settings', 'unlensed-file-image-1', value)

        else:
            golum_config.set('golum_settings', key, value)

    # set the prior settings
    golum_config.add_section('prior_settings')
    for key, value in config.items('prior_settings'):
        golum_config.set('prior_settings', key, value)

    # set the event settings
    golum_config.add_section('event_settings')
    for key, value in config.items('event_%i_settings'%n_img):
        golum_config.set('event_settings', key, value)

    # set the injection settings
    golum_config.add_section('injection_settings')
    for key, value in config.items('injection_settings_image_%i'%n_img):
        if 'injection-from-lensing' in key:
            golum_config.set('injection_settings', 'injection-from-lensing-parameters', value)
            # verify if this is true
            # if true, need to save the the parameters from the other image
            if value == 'True':
                val = config.getdict('injection_settings_image_%i'%n_comp, 'injection-dict')
                golum_config.set('injection_settings', 'other-image-parameters', str(val))

        else:
            golum_config.set('injection_settings', key, value)

    # set the condor settings
    golum_config.add_section('condor_settings')
    for key, value in config.items('condor_settings'):
        if key == 'submit':
            golum_config.set('condor_settings', 'submit', 'False')
        else:
            golum_config.set('condor_settings', key, value)

    # set the other settings
    golum_config.add_section('other_bilby_settings')
    for key, value in config.items('other_bilby_settings'):
        golum_config.set('other_bilby_settings', key, value)

    return golum_config

def make_injection_file_for_joint(config, n_img):
    """
    Function to make the injection file for one of the images
    when running golum joint PE

    ARGUMENTS:
    ----------
    - config: the config coming from the joint PE ini file
    - n_img: which image we want to analyze

    RETURNS:
    --------
    - injection_file: the injection file for the desired image
    """
    outd = config.get('output_settings', 'outdir')
    injection_file = f'{outd}/data/injection_golum_image{n_img}.dat'
    if n_img == 1:
        n_comp = 2
    elif n_img == 2:
        n_comp = 1

    # check how the injection need to be done
    trigg_time = None
    if config.getboolean('injection_settings_image_%i'%n_img, 'injection-from-lensing-parameters',fallback = False):
        if config.getboolean('injection_settings_image_%i'%n_comp, 'injection-from-lensing-parameters', fallback = False):
            raise ValueError("Cannot set the two images from lensing parameters")
        else:
            try:
                img1_params = config.getdict('injection_settings_image_%i'%n_comp, 'injection-dict')
                lensing_parameters = config.getdict('injection_settings_image_%i'%n_img, 'lensing-parameters-dict')
                # check which lensing parameters are given
                if 'relative_magnification' in lensing_parameters.keys():
                    inj_dict = golumutils.make_bbh_parameters_from_first_image_parameters_and_lensing_parameters(
                                        img1_params, lensing_parameters)
                else:
                    inj_dict = img1_params.copy()
                    for key in lensing_parameters.keys():
                        inj_dict[key] = lensing_parameters[key]
                trigg_time = inj_dict['geocent_time']
                prior_file = injection_file.replace(f'injection_golum_image{n_img}.dat', f'prior_golum_image{n_img}.dat')
                with open(prior_file, 'w', encoding = 'utf-8') as prior:
                    for key, value in inj_dict.items():
                        prior.write(f'{key}={value}\n')
            
            except:
                raise ValueError("Unable to setup the runs from lensing parameters")

    elif config.get('injection_settings_image_%i'%n_img, 'injection-dict')is None and config.get('injection_settings_image_%i'%n_img, 'injection-file') is not None:
        # use the injection file
        joint_pe_golum_logger.info('Using the provided injection file')
        inj_file = config.get('injection_settings', 'injection-file')
    elif config.getdict('injection_settings_image_%i'%n_img, 'injection-dict') is not None:
        joint_pe_golum_logger.info("Using provided injection dictionary")
        inj_dict = config.getdict('injection_settings_image_%i'%n_img, 'injection-dict')
        trigg_time = inj_dict['geocent_time']
        prior_file = injection_file.replace(f'injection_golum_image{n_img}.dat', f'prior_golum_image{n_img}.dat')
        with open(prior_file, 'w', encoding = 'utf-8') as prior:
            for key, value in inj_dict.items():
                prior.write(f'{key}={value}\n')
    else:
        raise ValueError("Injection parameters were not specified")

    # read the gps info if needed
    gps_tuple = config.get('analysis_settings', 'gps-tuple', fallback = None)
    gps_file = config.get('analysis_settings', 'gps-file', fallback = None)

    if gps_tuple is not None:
        gps_times = bilby_pipe.input.Input.parse_gps_tuple(gps_tuple)
    elif gps_file is not None:
        gps_times = bilby_pipe.input.Input.parse_gps_tuple(gps_file)
    else:
        gps_times = None

    n_injection = 1
    if trigg_time is None:
        trigger_time = config.getfloat('analysis_settings', 'trigger-time-image-%i'%n_img, fallback = None)
    else:
        trigger_time = trigg_time

    delta_t = config.getfloat('analysis_settings', 'deltaT', fallback = 0.2)
    duration = config.getfloat('analysis_settings', 'duration', fallback = 4)
    post_trigger_duration = config.getfloat('analysis_settings', 'post-trigger-duration', fallback = 2.)
    generation_seed = config.getint('injection_settings_image_%i'%n_img, 'generation-seed', fallback = None)
    enforce_signal_duration = config.getboolean('injection_settings_image_%i'%n_img, 'enforce-signal-duration', fallback = False)

    # use bilby to make the injection file
    bilby_pipe.create_injections.create_injection_file(injection_file,
                                                       prior_file = prior_file,
                                                       prior_dict = None,
                                                       n_injection = n_injection,
                                                       trigger_time = trigger_time,
                                                       deltaT = delta_t,
                                                       gpstimes = gps_times,
                                                       duration = duration, 
                                                       post_trigger_duration = post_trigger_duration,
                                                       generation_seed = generation_seed,
                                                       enforce_signal_duration = enforce_signal_duration)

    return injection_file


def make_waveform_generator_from_result_object(result):
    """
    Function making the waveform generator based on the 
    result from a golum/bilby run.

    ARGUMENTS:
    ----------
    - result: the bilby result object which should be read to get
              the waveform generator


    RETURNS:
    --------
    - waveform_generator: a bilby waveform generator identical to the
                          one used in the run
    """

    # set up the waveform arguments
    waveform_arguments = dict(waveform_approximant = result.meta_data['command_line_args']['waveform_approximant'],
                              reference_frequency = result.meta_data['command_line_args']['reference_frequency'])
    # be careful with minimum frequency
    try:
        waveform_arguments['minimum_frequency'] = float(result.meta_data['command_line_args']['minimum_frequency'])
    except ValueError:
        waveform_arguments['minimum_frequency'] = min(bilby_pipe.utils.convert_string_to_dict(result.meta_data['command_line_args']['minimum_frequency'], 'minimum-frequency').values())
    # same with the maximum frequenct 
    try:
        waveform_arguments['maximum_frequency'] = float(result.meta_data['command_line_args']['maximum_frequency'])
    except ValueError:
        waveform_arguments['maximum_frequency'] = max(bilby_pipe.utils.convert_string_to_dict(result.meta_data['command_line_args']['maximum_frequency'], 'maximum-frequency').values())

    # get frequency domain source model
    split_path = result.meta_data['command_line_args']['frequency_domain_source_model'].split(".")
    module = ".".join(split_path[:-1])
    freq_domain_fct = split_path[-1]
    freq_domain_source_model = getattr(import_module(module), freq_domain_fct)

    # load the conversion function 
    if result.meta_data['command_line_args']['conversion_function'] is None or result.meta_data['command_line_args']['conversion_function'] == 'None':
        conv_fct = bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters
    else:
        split_path = result.meta_data['command_line_args']['conversion_function'].split(".")
        module = '.'.join(split_path[:-1])
        fct = split_path[-1]
        conv_fct = getattr(import_module(module), fct)

    # get the waveform generator class
    spl_path = result.meta_data['command_line_args']['waveform_generator'].split(".")
    module = ".".join(spl_path[:-1])
    func = spl_path[-1]
    waveform_gen_fct = getattr(import_module(module), func)

    waveform_generator = waveform_gen_fct(duration = result.meta_data['command_line_args']['duration'],
                                          sampling_frequency = result.meta_data['command_line_args']['sampling_frequency'],
                                          frequency_domain_source_model = freq_domain_source_model,
                                          parameter_conversion = conv_fct,
                                          waveform_arguments = waveform_arguments)

    return waveform_generator



def send_email_alert(subject, message, sender, receivers):
    """
    A routine to send email alerts tracking the state of the jobs

    ARGUMENTS:
    ----------
    - subject: str, the email subject
    - message: str, the message content of the email
    - sender: str, email address of the person sending the email
    - receivers: list of email addresses to send the message to
    """

    full_email = f"Subject: {subject} \n\n {message}"

    email_object = smtplib.SMTP('localhost')
    email_object.sendmail(sender, receivers, full_email)
