"""
File with the scripts related to a conversion to 
bilby_pipe
"""

import os
import bilby_pipe
import golum
import bilby
from golum_pipe.core.utils import (golum_logger, golum_logger_img2, 
                                   symmetric_golum_logger, 
                                   joint_pe_golum_logger)
from golum_pipe.core import utils
import golum.tools.utils as golumutils

def make_bilby_pipe_config(config, args, output_directories, **kwargs):
    """
    Function reassembling all the arguments to make the configuration 
    dictionary handled by bilby_pipe

    ARGUMENTS:
    ----------
    - config: the ini configuration parser
    - args: the command line arguments
    - output_directories: the dictionary containing the places to send the
                          outputs to
    - **kwargs: ~ injection_file: path to file with injection data
                ~ injection_waveform_arguments: arguments to pass to the waveform
                                                for the injection
                ~ analysis_waveform_arguments: arguments to pass to the waveform    
                                               for the analysis
    
    RETURNS:
    --------
    - bilby_pipe_config: dictionary with the settings to be used
                         for bilby_pipe
    """

    additional_info = {"injection_file" : None,
                       "injection_waveform_arguments" : None,
                       "analysis_waveform_arguments" : None}
    additional_info.update(**kwargs)

    bilby_pipe_config = dict()
    bilby_pipe_config['local'] = args.local or config.getboolean('condor_settings', 'local', fallback = False)
    bilby_pipe_config['submit'] = False # we want to the dag file, not the runs to be started
    bilby_pipe_config['accounting'] = config.get('condor_settings', 'accounting', fallback = None)
    if bilby_pipe_config['accounting'] is None:
        golum_logger.warning('No accounting tag, job will NOT submit')

    for key, value in config.items('condor_settings'):
        if key == 'accounting' or key == 'submit' or key == 'local':
            continue

        if 'request-' in key:
            bilby_pipe_config[key] = value.replace('GB', '').replace('MB', '')
        else:
            bilby_pipe_config[key] = value

    bilby_pipe_config['label'] = config.get('output_settings', 'label')
    bilby_pipe_config['outdir'] = output_directories['outdir']
    for key, value in config.items('output_settings'):
        if key in ['outdir', 'label']:
            continue
        else:
            bilby_pipe_config[key] = value

    bilby_pipe_config['prior-file'] = config.get('prior_settings', 'prior-file', fallback = None)
    bilby_pipe_config['prior-dict'] = config.getdict('prior_settings', 'prior-dict', fallback = None)
    for key, value in config.items('prior_settings'):
        if key not in ['prior-file', 'prior-dict']:
            bilby_pipe_config[key] = value

    if 'analysis_settings' in config.sections():
        for key, value in config.items('analysis_settings'):
            bilby_pipe_config[key] = value
    else:
        # add dummy values to avoid issues later
        # this is used only for the joint analysis
        for key, value in config.items('analysis_settings_image_1'):
            bilby_pipe_config[key] = value

    for key, value in config.items('sampler_settings'):
        bilby_pipe_config[key] = value

    if 'event_settings' in config.sections():
        for key, value in config.items('event_settings'):
            bilby_pipe_config[key] = value
    else:
        # add dummy values to avoid issues later
        # this is used only for the joint analysis
        for key, value in config.items('event_1_settings'):
            bilby_pipe_config[key] = value

    for key, value in config.items('other_bilby_settings'):
        bilby_pipe_config[key] = value

    if 'type_II_search' in config._sections.keys():
        for key, value in config.items('type_II_search'):
            bilby_pipe_config[key] = value

    if 'golum_settings' in config._sections.keys():
        bilby_pipe_config['compute-coherence-ratio'] = config.getboolean('golum_settings', 'compute-coherence-ratio', fallback = False)
        if bilby_pipe_config['compute-coherence-ratio']:
            bilby_pipe_config['unlensed-file-image-1'] = config.get('golum_settings', 'unlensed-file-image-1')
            bilby_pipe_config['unlensed-file-image-2'] = config.get('golum_settings', 'unlensed-file-image-2')
    
    if 'overwrite-outdir' not in bilby_pipe_config:
        bilby_pipe_config['overwrite-outdir'] = True

    # check in case it is an injection 
    #bilby_pipe_config['injection'] = args.injection or config.getboolean('injection_settings', 'injection')
    if 'injection_settings' in config.sections():
        bilby_pipe_config['injection'] = config.getboolean('injection_settings', 'injection', fallback = False)
        if bilby_pipe_config['injection']:
            bilby_pipe_config['gaussian-noise'] = config.get('injection_settings', 'gaussian-noise', fallback = True)
            bilby_pipe_config['injection_file'] = additional_info['injection_file']
            bilby_pipe_config['injection_waveform_arguments'] = additional_info['injection_waveform_arguments']
            bilby_pipe_config['enforce-signal-duration'] = config.getboolean('injection_settings', 'enforce-signal-duration', fallback = False)
            bilby_pipe_config['generation-seed'] = config.getint('injection_settings', 'generation-seed', fallback = None)
            bilby_pipe_config['injection-waveform-approximant'] = config.get('injection_settings', 'injection-waveform-approximant', fallback = None)
        
    bilby_pipe_config['waveform_arguments_dict'] = additional_info['analysis_waveform_arguments']

    # add dummy detectors if needed
    if 'detectors' not in bilby_pipe_config.keys():
        joint_pe_golum_logger.warning("Adding dummy interferometers, this is normal only if you are doing a JPE run")
        detectors_img1 = config.get('analysis_settings', "detectors-image-1")
        detectors_img2 = config.get('analysis_settings', 'detectors-image-2')
        if len(detectors_img1) > len(detectors_img2):
            bilby_pipe_config['detectors'] = detectors_img1
            bilby_pipe_config['psd-dict'] = config.get('event_1_settings', 'psd-dict')
            
        else:
            bilby_pipe_config['detectors'] = detectors_img2
            bilby_pipe_config['psd-dict'] = config.get('event_2_settings', 'psd-dict')
            
    if 'population_settings' in config._sections.keys():
        for key, value in config.items('population_settings'):
            bilby_pipe_config[key] = value
    
    return bilby_pipe_config

def image2_args_to_bilby_pipe_config(config, args, output_directories, **kwargs):
    """
    Function reassembling all the arguments to make an ini
    file that can be handled by bilby pipe

    ARGUMENTS:
    ----------
    - config: the ini configuration parser
    - args: the command line arguments
    - output_directories: the dictionary containing the path
                          for the outputs
    - **kwargs: ~ injection_file: path to the file with the 
                                  injection data
                ~ injection_waveform_arguments: arguments to parse
                                                when generating the
                                                injection waveform
                ~ analysis_waveform_arguments: arguments to parse 
                                               when generating the 
                                               analysis waveform

    RETURNS:
    --------
    - bilby_pipe_config: dictionary with the settings to be used
                         for bilby_pipe
    """

    additional_info = {'injection_file' : None,
                       'injection_waveform_arguments' : None,
                       'analysis_waveform_arguments' : None}
    additional_info.update(**kwargs)

    bilby_pipe_config = dict()

    # setup the condor run information
    bilby_pipe_config['local'] = args.local or config.getboolean('condor_settings', 'local', fallback = False)
    bilby_pipe_config['submit'] = False
    bilby_pipe_config['accounting'] = config.get('condor_settings', 'accounting', fallback = None)
    if bilby_pipe_config['accounting'] is None:
        golum_logger_img2.warning('No accounting tag, job will NOT submit')
    for key, value in config.items('condor_settings'):
        if key == 'accounting':
            continue
        if 'request-' in key:
            bilby_pipe_config[key] = value.replace('GB', '') .replace('MB', '')
        else:
            bilby_pipe_config[key] = value

    # setting the output
    bilby_pipe_config['label'] = config.get('output_settings', 'label')
    bilby_pipe_config['outdir'] = output_directories['outdir']
    for key, value in config.items('output_settings'):
        if key in ['outdir', 'label']:
            continue
        else:
            bilby_pipe_config[key] = value

    # setup the elements for the modified likelihood
    bilby_pipe_config['likelihood-type'] = config.get('golum_settings', 'likelihood', fallback = golum.pe.likelihood.GravitationalWaveTransientImage2)

    # read in the posterior samples from the first image 
    img1_file = config.get('golum_settings', 'posterior-file-image-1')
    if os.path.isfile(img1_file):
        params_img1, posteriors_img1 = golumutils.read_image1_file(file = img1_file)
        # check if the Morse factor is in their, else
        # fill the array with dummy zeros
        if 'n_phase' not in posteriors_img1.keys():
            posteriors_img1['n_phase'] = [0 for i in range(len(posteriors_img1['geocent_time']))]
        # remove non-useful parameters and keep essential ones
        for key in list(posteriors_img1.keys()):
            # remove SNR info
            if 'matched_filter_snr' in key or 'optimal_snr' in key:
                posteriors_img1.pop(key)
            elif key in ['minimum_frequency', 'reference_frequency', 'time_jitter', 'waveform_approximant']:
                posteriors_img1.pop(key)
        if 'azimuth' in posteriors_img1.keys() and 'zenith' in posteriors_img1.keys() and 'ra' in posteriors_img1.keys() and 'dec' in posteriors_img1.keys():
            posteriors_img1.pop('azimuth')
            posteriors_img1.pop('zenith')
        if 'cos_tilt_1' in posteriors_img1.keys() and 'cos_tilt_2' in posteriors_img1.keys() and 'tilt_1' in posteriors_img1.keys() and 'tilt_2' in posteriors_img1.keys():
            posteriors_img1.pop('cos_tilt_1')
            posteriors_img1.pop('cos_tilt_2')
        if 'luminosity_distance' in posteriors_img1.keys() and 'comoving_distance' in posteriors_img1.keys():
            posteriors_img1.pop('comoving_distance')
        if 'luminosity_distance' in posteriors_img1.keys() and 'redshift' in posteriors_img1.keys():
            posteriors_img1.pop('redshift')
        if 'chirp_mass' in posteriors_img1.keys() and 'chirp_mass_source' in posteriors_img1.keys():
            posteriors_img1.pop('chirp_mass_source')
        if 'mass_1' in posteriors_img1.keys() and 'mass_1_source' in posteriors_img1.keys():
            posteriors_img1.pop('mass_1_source')
        if 'mass_2' in posteriors_img1.keys() and 'mass_2_source' in posteriors_img1.keys():
            posteriors_img1.pop('mass_2_source')
        if 'mass_1' in posteriors_img1.keys() and 'mass_2' in posteriors_img1.keys() and 'chirp_mass' in posteriors_img1.keys() and 'mass_ratio' in posteriors_img1.keys():
            posteriors_img1.pop('mass_1')
            posteriors_img1.pop('mass_2')
        if 'mass_ratio' in posteriors_img1.keys() and 'chirp_mass' in posteriors_img1.keys() and 'symmetric_mass_ratio' in posteriors_img1.keys():
            posteriors_img1.pop('symmetric_mass_ratio')
        if 'total_mass' in posteriors_img1.keys() and 'total_mass_source' in posteriors_img1.keys():
            posteriors_img1.pop('total_mass_source')
        if 'total_mass' in posteriors_img1.keys() and 'chirp_mass' in posteriors_img1.keys() and 'mass_ratio' in posteriors_img1.keys():
            posteriors_img1.pop('total_mass')
        if 'a_1' and 'a_2' in posteriors_img1.keys():
            if 'chi_1_in_plane' in posteriors_img1.keys():
                posteriors_img1.pop('chi_1_in_plane')
            if 'chi_2_in_plane' in posteriors_img1.keys():
                posteriors_img1.pop('chi_2_in_plane')
            if 'chi_eff' in posteriors_img1.keys():
                posteriors_img1.pop('chi_eff')
            if 'chi_p' in posteriors_img1.keys():
                posteriors_img1.pop('chi_p')
        if 'spin_1x' in posteriors_img1.keys() and 'a_1' in posteriors_img1.keys():
            posteriors_img1.pop('spin_1x')
            posteriors_img1.pop('spin_1y')
            posteriors_img1.pop('spin_1z')
            posteriors_img1.pop('spin_2x')
            posteriors_img1.pop('spin_2y')
            posteriors_img1.pop('spin_2z')
        if 'iota' in posteriors_img1.keys() and 'theta_jn' in posteriors_img1.keys():
            posteriors_img1.pop('iota')
        
    else:
        raise ValueError(f'{img1_file} is not an existing file')


    # need to take care of the priors here (for the likelihood, but also
    # for withing bilby_pipe)
    prior_file = config.get('prior_settings', 'prior-file', fallback = None)
    prior_dict = config.get('prior_settings', 'prior-dict', fallback = None)
    if prior_dict == 'None':
        prior_dict = None
    if prior_file == 'None':
        prior_file = None

    use_prior_for_look_up = config.getboolean('golum_settings', 'use-prior-for-look-up', fallback = True)
    time_img2 = config.getfloat('analysis_settings', 'trigger-time', fallback = None)
    time_img1 = config.getfloat('golum_settings', 'trigger-time-image-1', fallback = None)
    if time_img1 is None:
        golum_logger_img2.info('Image 1 trigger time not specified, trying from its result file')
        if params_img1 is not None:
            time_img1 = params_img1['geocent_time']
        else:
            raise ValueError('The prior on time delay cannot be set')
    bilby_pipe_config['trigger-time-image-1'] = time_img1            
    if time_img2 is None:
        # check if we can find the information from the dictionary
        try:
            lensing_params_dict = config.getdict('injection_settings', 'lensing-parameters-dict')
            dt_ = lensing_params_dict['delta_t']
            time_img2 = time_img1 + dt_
        except:
            time_img2 = None

    if use_prior_for_look_up:
        golum_logger_img2.info('Using the prior to reduce lookup table size')
        if prior_dict is None and prior_file is None:
            raise ValueError('You did not specify any prior')
        elif prior_dict is not None and prior_file is None:
            golum_logger_img2.info('Using prior dictionary')

            # use bilby_pipe to read the prior
            prior_dict = utils.read_prior(prior_dict, dictionary = True)
            if 'delta_t' not in prior_dict.keys():
                # use the trigger times to setup the prior
                dt = time_img2 - time_img1
                width_dt = config.getfloat('prior_settings', 'deltaDT', fallback = 0.2)
                prior_dict['delta_t'] = bilby.core.prior.Uniform(name = 'delta_t', minimum = dt - width_dt,
                                                                 maximum = dt + width_dt, latex_label = '$t_{21}$')
        elif prior_dict is None and prior_file is not None:
            golum_logger_img2.info('Using prior file')
            # use bilby_pipe to read the prior
            prior_dict = utils.read_prior(prior_file, file = True)
            if 'delta_t' not in prior_dict.keys():
                dt = time_img2 - time_img1
                width_dt = config.getfloat('prior_settings', 'deltaDT', fallback = 0.2)
                prior_dict['delta_t'] = bilby.core.prior.Uniform(name = 'delta_t', minimum = dt - width_dt,
                                                                 maximum = dt + width_dt, latex_label = '$t_{21}$')
    
    else:
        golum_logger_img2.info('Constructing look up table for all the times in data stream')
    # adjust the number of samples if requested by the user
    use_effective_parameters = config.getboolean('golum_settings', 'use-effective-parameters', fallback = False)
    n_samp = config.getint('golum_settings', 'n-samples', fallback = 2000)
    seed = config.get('golum_settings', 'sample-seed', fallback = -1)


    if seed == 'None':
        seed = None
    elif seed >= 0:
        seed = int(seed)
    else:
        seed = None
    extra_likely_kwargs_dict = {'posteriors' : posteriors_img1,
                                'n_samp' : n_samp,
                                'seed' : seed,
                                'use_effective_parameters' : use_effective_parameters}

    bilby_pipe_config['extra-likelihood-kwargs'] = str(extra_likely_kwargs_dict)

    # read the rest of the golum_seetings
    if 'golum_settings' in config._sections.keys():
        for key, value in config.items('golum_settings'):
            bilby_pipe_config[key] = value

    # load the priors in the pipe config
    bilby_pipe_config['prior-file'] = config.get('prior_settings', 'prior-file', fallback = None)
    bilby_pipe_config['prior-dict'] = config.getdict('prior_settings', 'prior-dict', fallback = None)

    for key, value in config.items('analysis_settings'):
        bilby_pipe_config[key] = value

    for key, value in config.items('sampler_settings'):
        bilby_pipe_config[key] = value

    for key, value in config.items('event_settings'):
        bilby_pipe_config[key] = value

    for key, value in config.items('other_bilby_settings'):
        bilby_pipe_config[key], value

    if 'overwrite-outdir' not in bilby_pipe_config:
        bilby_pipe_config['overwrite-outdir'] = True

    # check in case it is an injection 
    bilby_pipe_config['injection'] = args.injection or config.getboolean('injection_settings', 'injection')
    if bilby_pipe_config['injection']:
        bilby_pipe_config['gaussian-noise'] = config.get('injection_settings', 'gaussian-noise', fallback = True)
        bilby_pipe_config['injection_file'] = additional_info['injection_file']
        bilby_pipe_config['injection_waveform_arguments'] = additional_info['injection_waveform_arguments']
        bilby_pipe_config['n-simulation'] = config.getint('injection_settings', 'n-simulation', fallback = 1)
        bilby_pipe_config['generation-seed'] = config.getint('injection_settings', 'generation-seed', fallback = None)

    bilby_pipe_config['waveform_arguments_dict'] = additional_info['analysis_waveform_arguments']
    # check the trigger time 
    if 'trigger-time' not in bilby_pipe_config.keys() and time_img2 is not None:
        bilby_pipe_config['trigger-time'] = time_img2
    elif 'trigger-time' in bilby_pipe_config.keys():
        pass
    else:
        raise ValueError('No suitable way to set the trigger time')
    
    bilby_pipe_config['default-prior'] = 'PriorDict'
    bilby_pipe_config['enforce-signal-duration'] = False

    if 'population_settings'in config._sections.keys():
        bilby_pipe_config['selection-effects'] = config.getboolean('population_settings', 'selection-effects', fallback = False)
    else:
        # just switch off the population effects
        bilby_pipe_config['selection-effects'] = False

    return bilby_pipe_config

def symmetric_args_to_bilby_pipe_config(config, args, output_directories, img, **kwargs):
    """
    Function reassembling all the arguments to make an ini
    file that can be handled by bilby pipe when doing the symmetric run

    ARGUMENTS:
    ----------
    - config: the ini configuration parser
    - args: the command line arguments
    - output_directories: the dictionary containing the path
                          for the outputs
    - img: which image we will run pe on (for detectors)
    - **kwargs: ~ injection_file: path to the file with the 
                                  injection data
                ~ injection_waveform_arguments: arguments to parse
                                                when generating the
                                                injection waveform
                ~ analysis_waveform_arguments: arguments to parse 
                                               when generating the 
                                               analysis waveform

    RETURNS:
    --------
    - bilby_pipe_config: dictionary with the settings to be used
                         for bilby_pipe
    """
    if img == 1:
        img_comp = 2
    elif img == 2:
        img_comp = 1

    additional_info = {'injection_file' : None,
                       'injection_waveform_arguments' : None,
                       'analysis_waveform_arguments' : None}
    additional_info.update(**kwargs)

    bilby_pipe_config = dict()

    # setup the condor run information
    bilby_pipe_config['local'] = args.local or config.getboolean('condor_settings', 'local', fallback = False)
    bilby_pipe_config['submit'] = False
    bilby_pipe_config['accounting'] = config.get('condor_settings', 'accounting', fallback = None)
    if bilby_pipe_config['accounting'] is None:
        symmetric_golum_logger.warning('No accounting tag, job will NOT submit')
    for key, value in config.items('condor_settings'):
        if key == 'accounting':
            continue
        if 'request-' in key:
            bilby_pipe_config[key] = value.replace('GB', '') .replace('MB', '')
        else:
            bilby_pipe_config[key] = value

    # setting the output
    bilby_pipe_config['label'] = config.get('output_settings', 'label')
    bilby_pipe_config['outdir'] = output_directories['outdir']
    for key, value in config.items('output_settings'):
        if key in ['outdir', 'label']:
            continue
        else:
            bilby_pipe_config[key] = value

    # setup the elements for the modified likelihood
    bilby_pipe_config['likelihood-type'] = config.get('golum_settings', 'likelihood', fallback = golum.pe.likelihood.GravitationalWaveTransientImage2)

    # read in the posterior samples from the first image 
    img1_file = config.get('golum_settings', 'posterior-file-image-1')
    if os.path.isfile(img1_file):
        params_img1, posteriors_img1 = golumutils.read_image1_file(file = img1_file)
        # check if the Morse factor is in their, else
        # fill the array with dummy zeros
        if 'n_phase' not in posteriors_img1.keys():
            posteriors_img1['n_phase'] = [0 for i in range(len(posteriors_img1['geocent_time']))]
    else:
        raise ValueError(f'{img1_file} is not an existing file')


    # need to take care of the priors here (for the likelihood, but also
    # for withing bilby_pipe)
    prior_file = config.get('prior_settings', 'prior-file', fallback = None)
    prior_dict = config.get('prior_settings', 'prior-dict', fallback = None)
    if prior_dict == 'None':
        prior_dict = None
    if prior_file == 'None':
        prior_file = None

    use_prior_for_look_up = config.getboolean('golum_settings', 'use-prior-for-look-up', fallback = True)
    time_img2 = config.getfloat('analysis_settings', 'trigger-time', fallback = None)
    time_img1 = config.getfloat('golum_settings', 'trigger-time-image-1', fallback = None)
    if time_img1 is None:
        symmetric_golum_logger.info('Image 1 trigger time not specified, trying from its result file')
        if params_img1 is not None:
            time_img1 = params_img1['geocent_time']
        else:
            raise ValueError('The prior on time delay cannot be set')
    bilby_pipe_config['trigger-time-image-1'] = time_img1            
    if time_img2 is None:
        # check if we can find the information from the dictionary
        try:
            lensing_params_dict = config.getdict('injection_settings', 'lensing-parameters-dict')
            dt_ = lensing_params_dict['delta_t']
            time_img2 = time_img1 + dt_
        except:
            time_img2 = None

    if use_prior_for_look_up:
        symmetric_golum_logger.info('Using the prior to reduce lookup table size')
        if prior_dict is None and prior_file is None:
            raise ValueError('You did not specify any prior')
        elif prior_dict is not None and prior_file is None:
            golum_logger_img2.info('Using prior dictionary')

            # use bilby_pipe to read the prior
            prior_dict = utils.read_prior(prior_dict, dictionary = True)
            if 'delta_t' not in prior_dict.keys():
                # use the trigger times to setup the prior
                dt = time_img2 - time_img1
                width_dt = config.getfloat('prior_settings', 'deltaDT', fallback = 0.2)
                prior_dict['delta_t'] = bilby.core.prior.Uniform(name = 'delta_t', minimum = dt - width_dt,
                                                                 maximum = dt + width_dt, latex_label = '$t_{21}$')
        elif prior_dict is None and prior_file is not None:
            golum_logger_img2.info('Using prior file')
            # use bilby_pipe to read the prior
            prior_dict = utils.read_prior(prior_file, file = True)
            if 'delta_t' not in prior_dict.keys():
                dt = time_img2 - time_img1
                width_dt = config.getfloat('prior_settings', 'deltaDT', fallback = 0.2)
                prior_dict['delta_t'] = bilby.core.prior.Uniform(name = 'delta_t', minimum = dt - width_dt,
                                                                 maximum = dt + width_dt, latex_label = '$t_{21}$')
    
    else:
        symmetric_golum_logger.info('Constructing look up table for all the times in data stream')
    # adjust the number of samples if requested by the user
    use_effective_parameters = config.getboolean('golum_settings', 'use-effective-parameters', fallback = False)
    n_samp = config.getint('golum_settings', 'n-samples', fallback = 2000)
    seed = config.get('golum_settings', 'sample-seed', fallback = -1)


    if seed == 'None':
        seed = None
    elif seed >= 0:
        seed = int(seed)
    else:
        seed = None
    extra_likely_kwargs_dict = {'posteriors' : posteriors_img1,
                                'n_samp' : n_samp,
                                'seed' : seed,
                                'use_effective_parameters' : use_effective_parameters}

    bilby_pipe_config['extra-likelihood-kwargs'] = str(extra_likely_kwargs_dict)

    # read the rest of the golum_seetings
    if 'golum_settings' in config._sections.keys():
        for key, value in config.items('golum_settings'):
            bilby_pipe_config[key] = value

    # load the priors in the pipe config
    bilby_pipe_config['prior-file'] = config.get('prior_settings', 'prior-file', fallback = None)
    bilby_pipe_config['prior-dict'] = config.getdict('prior_settings', 'prior-dict', fallback = None)

    for key, value in config.items('analysis_settings'):
        if key == "detectors-image-%i"%img:
            bilby_pipe_config['detectors'] = value
        elif key == "detectors-image-%i"%img_comp:
            continue
        else:
            bilby_pipe_config[key] = value

    for key, value in config.items('sampler_settings'):
        bilby_pipe_config[key] = value

    for key, value in config.items('event_settings'):
        bilby_pipe_config[key] = value

    for key, value in config.items('other_bilby_settings'):
        bilby_pipe_config[key], value

    if 'overwrite-outdir' not in bilby_pipe_config:
        bilby_pipe_config['overwrite-outdir'] = True

    # check in case it is an injection 
    bilby_pipe_config['injection'] = args.injection or config.getboolean('injection_settings', 'injection')
    if bilby_pipe_config['injection']:
        bilby_pipe_config['gaussian-noise'] = config.get('injection_settings', 'gaussian-noise', fallback = True)
        bilby_pipe_config['injection_file'] = additional_info['injection_file']
        bilby_pipe_config['injection_waveform_arguments'] = additional_info['injection_waveform_arguments']
        bilby_pipe_config['n-simulation'] = config.getint('injection_settings', 'n-simulation', fallback = 1)
        bilby_pipe_config['generation-seed'] = config.getint('injection_settings', 'generation-seed', fallback = None)

    bilby_pipe_config['waveform_arguments_dict'] = additional_info['analysis_waveform_arguments']
    # check the trigger time 
    if time_img2 is not None:
        bilby_pipe_config['trigger-time'] = time_img2
    else:
        raise ValueError('No suitable way to set the trigger time')
    
    bilby_pipe_config['default-prior'] = 'PriorDict'
    bilby_pipe_config['enforce-signal-duration'] = False

    # put the final selection effects to false here
    # for symmetric runs, they are computed on the joint posteriors
    bilby_pipe_config['selection-effects'] = False

    return bilby_pipe_config


def get_bilby_pipe_arguments(bilby_pipe_config):
    """
    Function making the necessary arguments to pass to the 
    bilby functions

    ARGUMENTS:
    ----------
    - bilby_pipe_config: dictionary with the bilby settings

    RETURN:
    -------
    - bilby_pipe_args: arguments known to bilby_pipe
    - bilby_pipe_unknown_args: arguments unknown to bilby_pipe
    - bilby_pipe_parser: argument parser specific to bilby_pipe
    """

    bilby_pipe_parser = bilby_pipe.parser.create_parser()
    outd = bilby_pipe_config['outdir']
    bilby_pipe_ini = f'{outd}/bilbypipe.ini'

    argument_list = [bilby_pipe_ini]

    with open(bilby_pipe_ini, 'w', encoding = 'utf-8') as inif:
        for key, value in bilby_pipe_config.items():
            inif.writelines(f'{key} = {value} \n')

    bilby_pipe_args, bilby_pipe_unknown_args = bilby_pipe_parser.parse_known_args(argument_list)

    return bilby_pipe_args, bilby_pipe_unknown_args, bilby_pipe_parser
