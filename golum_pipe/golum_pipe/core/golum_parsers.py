"""
Various functions to parse the golum arguments
"""

import sys
import argparse

def set_golum_parser():
    """
    Function setting up the GOLUM parser based on the command
    line
    """

    if 'first_image' in sys.argv[0]:
        golumparser = first_image_golum_parser()
    elif 'second_image' in sys.argv[0]:
        golumparser = second_image_golum_parser()
    elif 'symmetric' in sys.argv[0]:
        golumparser = symmetric_golum_parser()
    elif 'joint' in sys.argv[0]:
        golumparser = joint_pe_golum_parser()
    else:
        raise ValueError(f'{sys.argv[0]} not recognized as a golum parser')

    return golumparser


def first_image_golum_parser():
    """
    Function setting up the arguments and parsing them
    for the first image run
    """

    prog = 'golum_first_image'
    description = "golum analysis for the first lensed image"

    parser = argparse.ArgumentParser(prog = prog, description = description)

    # parse the ini file
    parser.add_argument('ini', metavar = 'INI', type = str,
                        help = 'Location of the ini file containing the information for the run')


    # maybe add some options here later
    parser.add_argument('-l', '--local', action = 'store_true',
                        help = 'If used, the run will be done locally')

    parser.add_argument('-i', '--injection', action='store_true',
                        help = 'If used, the run will be injection')

    # optional utility argument
    parser.add_argument('-v', '--verbose', action = 'store_true',
                        help = 'If used, displays the maximum information')

    return parser

def second_image_golum_parser():
    """
    Function setting up the arguments ans parsing them for
    the second golum run 
    """

    prog = 'golum_second_image'
    description = 'golum analysis for the second lensed image'

    parser = argparse.ArgumentParser(prog = prog, description = description)

    # parse the ini file
    parser.add_argument('ini', metavar = 'INI', type = str,
                        help = 'Location of the ini file containing the information for the run')

  
    # maybe add some options here later
    parser.add_argument('-l', '--local', action = 'store_true',
                        help = 'If used, the run will be done locally')
    
    parser.add_argument('-i', '--injection', action='store_true',
                        help = 'If used, the run will be injection')


    # optional utility argument
    parser.add_argument('-v', '--verbose', action = 'store_true',
                        help = 'If used, displays the maximum information')

    return parser

def symmetric_golum_parser():
    """
    Function setting up the arguments ans parsing them for
    the second golum run 
    """

    prog = 'golum_symmetric'
    description = 'Symmetric golum analysis'

    parser = argparse.ArgumentParser(prog = prog, description = description)

    # parse the ini file
    parser.add_argument('ini', metavar = 'INI', type = str,
                        help = 'Location of the ini file containing the information for the run')


    # maybe add some options here later
    parser.add_argument('-l', '--local', action = 'store_true',
                        help = 'If used, the run will be done locally')

    parser.add_argument('-i', '--injection', action='store_true',
                        help = 'If used, the run will be injection')

    # optional utility argument
    parser.add_argument('-v', '--verbose', action = 'store_true',
                        help = 'If used, displays the maximum information')

    return parser

def joint_pe_golum_parser():
    """
    Function setting up the arguments ans parsing them for
    the second golum run 
    """

    prog = 'golum_joint_pe'
    description = 'Joint parameter estimation GOLUM run'

    parser = argparse.ArgumentParser(prog = prog, description = description)

    # parse the ini file
    parser.add_argument('ini', metavar = 'INI', type = str,
                        help = 'Location of the ini file containing the information for the run')


    # maybe add some options here later
    parser.add_argument('-l', '--local', action = 'store_true',
                        help = 'If used, the run will be done locally')
    parser.add_argument('-i', '--injection', action='store_true',
                        help = 'If used, the run will be injection')


    # optional utility argument
    parser.add_argument('-v', '--verbose', action = 'store_true',
                        help = 'If used, displays the maximum information')

    return parser
