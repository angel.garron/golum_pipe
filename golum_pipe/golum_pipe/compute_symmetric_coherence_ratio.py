"""
Script to initial the symmetric coherence ratio computation
"""

import re
import sys
from golum_pipe.postprocessing.symmetric_clu_computation import main

if __name__ == "__main__":
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(main())