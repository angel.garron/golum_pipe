"""
Some utility function for custom setups when needed
"""
import os
import shutil

def make_sub_file(case, sub_dir, executable, label, accounting_group, memory = 8, disk = 6, cpus = 1):
    """
    Function to write the sub file for the specified executable
    """
    log_dir = sub_dir.replace('submit', 'log_data_analysis')
    subfile_name = f"{sub_dir}/{case}_{executable}_{label}.sub"
    subfile = open(subfile_name, 'w')
    subfile.write("universe = vanilla \n")
    exe_path = shutil.which(executable)
    subfile.write(f"executable = {exe_path} \n")
    subfile.write(f"request_memory = {memory}GB \n")
    subfile.write(f"request_disk = {disk}GB \n")
    subfile.write(f"request_cpus = {cpus} \n")
    subfile.write("getenv = True \n")
    initialdir = os.getcwd()
    subfile.write(f"initialdir = {initialdir} \n")
    subfile.write("notifications = Never \n")
    subfile.write("requirements = \n")
    subfile.write(f"log = {log_dir}/{case}_{executable}_{label}.log \n")
    subfile.write(f"output = {log_dir}/{case}_{executable}_{label}.out \n")
    subfile.write(f"error = {log_dir}/{case}_{executable}_{label}.err \n")
    subfile.write(f"accounting_group = {accounting_group} \n")
    subfile.write("priority = 0 \n")
    subfile.write("arguments = $(ARGS) \n")
    subfile.write("queue")
    subfile.close()

    return subfile_name

def setup_symmetric_reweighting(args, config, output_directories, dag):
    """
    Function to set up the symmetric reweighting without the need to pass
    via a formal Bilby dag. This is just easier in this case

    ARGUMENTS:
    ----------
    - args: the symmetric arguments
    - config: the symmetric config 
    - output_directories: dictionary containing the basic output information 
    - dag: the dag file in which to write the information
    """
    sub_dir = f'{output_directories["outdir"]}/submit'
    label = config.get('output_settings', 'label')
    accounting_group = config.get('condor_settings', 'accounting')

    sub_file = make_sub_file('symmetric_reweight', sub_dir, 'golum_symmetric_reweight', label, accounting_group)

    job_name = f'symmetric_reweighting_{label}'
    ini_file = args.ini
    dag.write(f"JOB {job_name} {sub_file} \n")
    dag.write(f'VARS {job_name} ARGS="--ini-file {ini_file}" \n')

def setup_symmetric_clu_reweighting(args, config, output_directories, dag):
    """
    Function to set up the symmetric clu computation without the need to pass
    via a formal Bilby dag. This is just easier in this case. This is directly
    re-written in the dag.

    ARGUMENTS:
    ----------
    - args: the arguments
    - config: the symmetric config 
    - output_directories: dictionary containing the basic output information 
    - dag: the dag file in which to write the information
    """

    sub_dir = f'{output_directories["outdir"]}/submit'
    label = config.get('output_settings', 'label')
    accounting_group = config.get('condor_settings', 'accounting')

    sub_file = make_sub_file('symmetric_reweight', sub_dir, 'golum_symmetric_clu_computation', label, accounting_group)

    job_name = f'symmetric_compute_clu_{label}'
    ini_file = args.ini
    dag.write(f"JOB {job_name} {sub_file} \n")
    dag.write(f'VARS {job_name} ARGS="--ini-file {ini_file}" \n')

def setup_population_reweighting(case, args, config, output_directories, dag):
    """
    Function to set up the population reweighting process to include selection effects

    ARGUMENTS:
    ---------
    - case: whether we want the population reweighting for the symmetric or
            joint case
    - args: the arguments coming from the run
    - config: the config file initially parsed to the run 
    - output_directories: the directory where we need to output the information
    - dag: the dag file in which we want to write the information for the run
    """

    sub_dir = f'{output_directories["outdir"]}/submit'
    label = config.get('output_settings', 'label')
    accounting_group = config.get('condor_settings', 'accounting')
    lens_mem = config.getint('population_settings', 'pop-request-memory', fallback = 8)
    lens_disk = config.getint('population_settings', 'pop-request-disk', fallback = 6)
    lens_cpus = config.getint('population_settings', 'pop-request-cpus', fallback = 16)
    
    sub_file = make_sub_file(f'{case}_selection_effects', sub_dir, 'golum_compute_bayes_factor', label, accounting_group,
                             lens_mem, lens_disk, lens_cpus)
    job_name = f'{case}_selection_effects_{label}'
    ini_file = args.ini
    dag.write(f"JOB {job_name} {sub_file} \n")
    dag.write(f'VARS {job_name} ARGS="--ini-file {ini_file} --case {case}" \n')
