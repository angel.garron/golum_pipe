"""
File with the modified nodes for generation and analysis
"""

from bilby_pipe.job_creation.node import Node
from bilby_pipe.job_creation.nodes import (
    AnalysisNode,
    FinalResultNode,
    GenerationNode,
    MergeNode,
    PESummaryNode,
    PlotNode,
    PostProcessAllResultsNode,
    PostProcessSingleResultsNode,
)
from bilby_pipe.utils import ArgumentsString
from golum_pipe.core.utils import symmetric_golum_logger, joint_pe_golum_logger
import bilby
import os
import bilby_pipe

# make the Golum generation node
class GolumImage2GenerationNode(GenerationNode):
    """
    Overwrites the bilby_pipe generation node to link to the correct 
    job for the generation
    """

    def __init__(self, inputs, trigger_time, idx, dag, parent = None):
        super().__init__(inputs, trigger_time, idx, dag, parent)


    @property
    def executable(self):
        """
        Function to grab the executable
        """
        
        return self._get_executable_path("golum_image2_generation")

class GolumImage2AnalysisNode(AnalysisNode):
    """
    Overwrites the bilby_pipe analysis node to adapt the
    executable
    """

    def __init__(self, inputs, generation_node, detectors, sampler, parallel_idx, dag):
        super().__init__(inputs, generation_node, detectors, sampler, parallel_idx, dag)

    @property
    def executable(self):
        """
        Take the correct golum executable
        """

        return self._get_executable_path("golum_image2_analysis")

class GolumPlotNode(PlotNode):
    """
    Class overwriting the plotting node
    to use the generic plotting as to always have 
    a corner plot with all the parameters without
    calibration, log_prior, log_likelihood
    """

    def __init__(self, inputs, merged_node, dag):
        super().__init__(inputs, merged_node, dag)

    @property
    def executable(self):
        """
        Use the adapted golum executable
        """
        return self._get_executable_path('golum_generic_plot')

class BayesTypeIINode(Node):
    """
    Class to add the computation of the Bayes factor for
    type II images to the list of jobs after the first 
    image PE is done.
    """
    def __init__(self, inputs, parent_node, dag):
        super().__init__(inputs)
        self.dag = dag
        self.request_cpus = 1
        self.job_name = f'{parent_node.label}_typeII_bayes_factor'

        self.setup_arguments(
            add_ini=False, add_unknown_args=False, add_command_line_args=False
        )

        self.arguments.add("result", parent_node.result_file)
        self.arguments.add("outdir", inputs.final_result_directory)
    
        self.process_node()
        self.job.add_parent(parent_node.job)

    @property
    def executable(self):
        return self._get_executable_path('golum_typeII_bayes_factor')

    @property
    def request_memory(self):
        return "4 GB"

    @property
    def log_directory(self):
        return self.inputs.data_analysis_log_directory

class ImagePairReweightingNode(Node):
    """
    Class to add the reweighting to get the joint posteriors
    after the run has been done
    """
    def __init__(self, inputs, parent_node, dag):
        super().__init__(inputs)
        self.dag = dag
        self.request_cpus = 1
        self.job_name = f'{parent_node.label}_joint_posterior_reweighting'

        self.setup_arguments(
            add_ini=False, add_unknown_args=False, add_command_line_args=False
        )

        self.arguments.add("result", parent_node.result_file)
        self.arguments.add("outdir", inputs.final_result_directory)
        self.arguments.add_unknown_args(inputs.unknown_args)
        self.label = parent_node.label
        self.result_file = parent_node.result_file
        self.process_node()
        self.job.add_parent(parent_node.job)

    @property
    def executable(self):
        return self._get_executable_path('golum_reweight_joint_samples')

    @property
    def request_memory(self):
        return "8 GB"

    @property
    def log_directory(self):
        return self.inputs.data_analysis_log_directory


class CoherenceRationComputationNode(Node):
    """
    Class to add the computation of the coherence ratio 
    to the final result as a child job of the second image
    run
    """
    def __init__(self, inputs, parent_node, dag):
        super().__init__(inputs)
        self.dag = dag
        self.request_cpus = 1
        self.job_name = f'{parent_node.label}_coherence_ratio_computation'

        self.setup_arguments(
            add_ini=False, add_unknown_args=False, add_command_line_args=False
        )

        self.arguments.add("result", parent_node.result_file)
        self.arguments.add("outdir", inputs.final_result_directory)
        self.arguments.add_unknown_args(inputs.unknown_args)
        
        self.process_node()
        self.job.add_parent(parent_node.job)

    @property
    def executable(self):
        return self._get_executable_path('golum_compute_coherence_ratio')

    @property
    def request_memory(self):
        return "8 GB"

    @property
    def log_directory(self):
        return self.inputs.data_analysis_log_directory


class GolumRunsNode(Node):
    """
    Custom node to start the GOLUM runs in 
    symmetric Golum runs. 
    """

    def __init__(self, inputs, dag, golum_ini_file, n_img):
        """
        Node for GOLUM run starting

        ARGUMENTS:
        ----------
        - inputs: the inputs needed for the run 
        - dag: the dag structure 
        - golum_ini_file: the ini file created from the 
                          symmetric one and corresponding 
                          to the GOLUM run of interest
        """
        if n_img == 1:
            n_comp = 2
        else:
            n_comp = 1

        super().__init__(inputs)
        self.dag = dag
        self.golum_ini_file = golum_ini_file
        self.inputs.request_cpus = 1
        self.request_cpus = 1
        self.job_name = f'{inputs.label}_symmetric_golum_run_img{n_img}img{n_comp}'
        self.setup_arguments()
        self.process_node()

    def setup_arguments(self, parallel_program = None, add_command_line_args = True, add_ini = True, add_unknown_args = False,):
        """
        Overwrite the initial function to actually have the ini file
        from the GOLUM run to be added in the arguments rather than 
        the complete ini.

        Also setting the unknown args to false. They are not useful here
        as only the ini file is required to start the jon 
        """
        self.arguments = ArgumentsString()
        if parallel_program is not None:
            self.arguments.add("np", self.inputs.request_cpus)
            self.arguments.add_positional_argument(parallel_program)
        if add_ini:
            self.arguments.add_positional_argument(self.golum_ini_file)
        if add_unknown_args:
            self.arguments.add_unknown_args(self.inputs.unknown_args)
        if add_command_line_args:
            self.arguments.add_command_line_arguments()

    @property
    def executable(self):
        return self._get_executable_path('golum_second_image')

    # avoid requesting unuseful memory
    @property
    def request_memory(self):
        return "4 GB"

    @property
    def log_directory(self):
        return self.inputs.data_analysis_log_directory

class GolumJointAnalysisNode(Node):
    """
    Making an analysis node able to deal with joint PE. 
    In this case, we want to change the executable but also
    pass the individual ini files to the joint PE analysis. 
    """

    def __init__(self, joint_inputs, generation_node_img1, generation_node_img2, parallel_idx, sampler, dag, img1_ini, img2_ini):
        """
        Additional elements to the ini

        - joint_inputs: is the main input from which the run information will be taken
        - generation_node_img1: the generation node for the first image
        - generation_node_img2: the generation node for the second image
        - img1_ini: the complete ini file for the first image from which we will read 
                    the information for this event (such as the injection file, detectors, ...)
        - img2_ini: the complete ini file for the first image from which we will read 
                    the information for this event (such as the injection file, detectors, ...)

        Removed the detectors argument as we need to have the individual detectors
        for each image
        """
        super().__init__(joint_inputs)

        self.dag = dag
        self.generation_node_img1 = generation_node_img1
        self.generation_node_img2 = generation_node_img2
        self.parallel_idx = parallel_idx
        self.request_cpus = joint_inputs.request_cpus
        self.joint_inputs = joint_inputs


        self.base_job_name = joint_inputs.label+"joint_analysis"

        if parallel_idx != "":
            self.job_name = f"{self.base_job_name}_{parallel_idx}"
        else:
            self.job_name = self.base_job_name

        self.label = self.job_name

        if self.joint_inputs.use_mpi:
            self.setup_arguments(parallel_program = self._get_executable_path(self.joint_inputs.analysis_executable),
                                 add_unknown_args = False)
        else:
            self.setup_arguments(add_unknown_args = False)

        if self.joint_inputs.transfer_files or self.joint_inputs.osg:
            data_dump_file_img1 = generation_node_img1.data_dump_file
            data_dump_file_img2 = generation_node_img2.data_dump_file
            input_files_to_transfer = [str(data_dump_file_img1),
                                       str(data_dump_file_img2),
                                       str(self.joint_inputs.complete_ini_file)]
            self.extra_lines.extend(self._condor_file_transfer_lines(
                                    input_files_to_transfer,
                                    [self._relative_topdir(self.joint_inputs.outdir, self.joint_inputs.initialdir)],))
            self.arguments.add('outdir', os.path.relpath(self.joint_inputs.outdir))

        self.arguments.add("label", self.label)
        self.arguments.add("data-dump-file-img1", generation_node_img1.data_dump_file)
        self.arguments.add("data-dump-file-img2", generation_node_img2.data_dump_file)
        self.arguments.add("sampler", sampler)
        self.arguments.add("ini-file-img1", img1_ini)
        self.arguments.add("ini-file-img2", img2_ini)

        # add the full prior as dictionary 
        prior_file = self.make_prior_file()
        self.arguments.add("joint-prior-file", prior_file)
        
        self.extra_lines.extend(self._checkpoint_submit_lines())
        if self.request_cpus > 1:
            self.extra_lines.extend(['environment = "OMP_NUM_THREADS=1"'])

        self.process_node()
        self.job.add_parent(generation_node_img2.job)

    def make_prior_file(self):
        """
        Function generating a prior file and parsing it as an argument
        to the run. 
        """
        prior_class = bilby.gw.prior.PriorDict
        try:
            prior_dict_str = bilby_pipe.utils.convert_prior_string_input(self.prior_file)
            prior_dict_str = {bilby_pipe.input.Input._convert_prior_dict_key(key): val
                          for key, val in prior_dict.items()}
            prior_dict = prior_class(dictionary = prior_dict_str)
            prior_file_name = None

        except AttributeError:
            try:
                prior_file = self.joint_inputs.prior_file
                prior_file_name = prior_file
                # check if we can access the file
                if os.path.isfile(prior_file):
                    pass
                elif os.path.isfile(os.path.basename(prior_file)):
                    prior_file = os.path.basename(prior_file)
                else:
                    raise ValueError(f"The specified prior file {prior_file} is not found")

                prior_dict = prior_class(filename = prior_file)
            except:
                raise ValueError("No way to find the prior to be passed to the joint analysis")

        # add the missing time-related priors
        if 'geocent_time' in prior_dict.keys() or 'geocent_time_1' in prior_dict.keys():
            joint_pe_golum_logger.info("Using the time prior specified by user for the first image")
        else:
            time_prior = self.set_time_prior_img_1()
            if 'relative_magnification' in prior_dict.keys():
                prior_dict['geocent_time'] = time_prior
            else:
                prior_dict['geocent_time_1'] = time_prior

        if 'geocent_time_2' in prior_dict.keys() or 'delta_t' in prior_dict.keys():
            joint_pe_golum_logger.info("Using the time prior specified by used for the second image")
        elif 'relative_magnification' in prior_dict.keys():
            joint_pe_golum_logger.info("Setting prior for time delay, using relative lensing parameters")
            time_prior_img2 = self.set_img2_time_prior(delta_t = True)
            prior_dict['delta_t'] = time_prior_img2
        else:
            joint_pe_golum_logger.info('Setting prior for geocentric time second image, using apparent parameters')
            time_prior_img2 = self.set_img2_time_prior(geocent_time = True)
            prior_dict['geocent_time_2'] = time_prior_img2

        prior_file_name = f'{self.label}_prior.json'
        prior_dict.to_json(self.joint_inputs.outdir, self.label)

        return os.path.join(self.joint_inputs.outdir, prior_file_name)


    def set_time_prior_img_1(self):
        """
        Function to setup the time prior. 
        It is redefined here to use the injection file if the 
        trigger times have not been added

        RETURNS:
        --------
        - time_prior: the usual geocentric time prior
        """
       
        try:
            trigg_time = None
            for st in self.joint_inputs.unknown_args:
                if 'trigger-time-image-1' in st:
                    trigg_time = float(st.split('=')[-1])

            if trigg_time == None:
                raise ValueError('No trigger time specified for the first image')

        except:
            raise ValueError('No trigger time specified for the first image')

        time_prior = bilby_pipe.utils.get_time_prior(time = trigg_time,
                                                     uncertainty = self.joint_inputs.deltaT/2.,
                                                     name = "geocent_time_1",
                                                     latex_label = r'$t_c^1$')
        
        return time_prior

    def set_img2_time_prior(self, delta_t = False, geocent_time = False):
        """
        Function to set up the time prior for the second image. 
        It will either set it up for the time delay or for the 
        geocentric time. This is chosen based on the other priors
        passed

        ARGUMENTS:
        ----------
        - delta_t: bool, default is False: whether we want the prior for
                   the time delay between the images
        - geocent_time: bool, default is False: Whether we wane the prior to
                        be the geocentric time prior for the second image

        RETURNS:
        --------
        - time_prior: the time-related prior for the second image
        """

        if delta_t == geocent_time:
            raise ValueError("You have to chose whether you want a time delay or a geocentric time prior for the second image")

        trigg_time_2 = None
        for st in self.joint_inputs.unknown_args:
            if 'trigger-time-image-2' in st:
                trigg_time_2 = float(st.split('=')[-1])

        if trigg_time_2 == None:
            raise ValueError('No trigger time specified for the first image')

        if geocent_time is True:
            time_prior = bilby_pipe.utils.get_time_prior(time = trigg_time_2,
                                                         uncertainty = self.joint_inputs.deltaT/2.,
                                                         name = 'geocent_time_2',
                                                         latex_label = r'$t_c^2$')
        else:
            trigg_time_1 = None
            for st in self.joint_inputs.unknown_args:
                if 'trigger-time-image-1' in st:
                    trigg_time_1 = float(st.split('=')[-1])

            if trigg_time_1 == None:
                raise ValueError('No trigger time specified for the first image')

            dt_trigg = trigg_time_2 - trigg_time_1
            time_prior = bilby_pipe.utils.get_time_prior(time = dt_trigg,
                                                         uncertainty = self.joint_inputs.deltaT,
                                                         name = 'delta_t',
                                                         latex_label = r'$\Delta t$')

        return time_prior

    @property
    def executable(self):
        return self._get_executable_path("golum_joint_pe_analysis")

    @property
    def request_memory(self):
        return self.joint_inputs.request_memory

    @property
    def log_directory(self):
        return self.joint_inputs.data_analysis_log_directory

    @property
    def result_file(self):
        return f"{self.joint_inputs.result_directory}/{self.job_name}_result.{self.joint_inputs.result_format}"

    @property
    def slurm_walltime(self):
        """Default wall-time for base-name"""
        # Seven days
        return self.joint_inputs.scheduler_analysis_time

class GolumPEsummaryNode(Node):
    """
    Node adapted to replace the PE summary node when doing JPE
    """

    def __init__(self, inputs, merged_node_list, generation_node_list_img1, generation_node_list_img2, dag):
        super().__init__(inputs)
        self.dag = dag
        self.job_name = f"{inputs.label}_pesummary"
        self.request_cpus = 1

        n_results = len(merged_node_list)
        result_files = [merged_node.result_file for merged_node in merged_node_list]
        labels = [merged_node.label for merged_node in merged_node_list]

        self.setup_arguments(
            add_ini=False, add_unknown_args=False, add_command_line_args=False
        )
        self.arguments.add("webdir", self.inputs.webdir)
        if self.inputs.email is not None:
            self.arguments.add("email", self.inputs.email)
        self.arguments.add(
            "config", " ".join([self.inputs.complete_ini_file] * n_results)
        )
        self.arguments.add("samples", f"{' '.join(result_files)}")

        # Using append here as summary pages doesn't take a full name for approximant
        self.arguments.append("-a")
        self.arguments.append(" ".join([self.inputs.waveform_approximant] * n_results))

        if len(generation_node_list_img1) == 1:
            self.arguments.add("gwdata_img1", generation_node_list_img1[0].data_dump_file)
        elif len(generation_node_list_img1) > 1:
            logger.info(
                "Not adding --gwdata_img1 to PESummary job as there are multiple files"
            )
        if len(generation_node_list_img2) == 1:
            self.arguments.add("gwdata_img2", generation_node_list_img2[0].data_dump_file)
        elif len(generation_node_list_img2) > 1:
            logger.info(
                "Not adding --gwdata_img2 to PESummary job as there are multiple files"
            )
        existing_dir = self.inputs.existing_dir
        if existing_dir is not None:
            self.arguments.add("existing_webdir", existing_dir)

        if isinstance(self.inputs.summarypages_arguments, dict):
            if "labels" not in self.inputs.summarypages_arguments.keys():
                self.arguments.append(f"--labels {' '.join(labels)}")
            else:
                if len(labels) != len(result_files):
                    raise BilbyPipeError(
                        "Please provide the same number of labels for postprocessing "
                        "as result files"
                    )
            not_recognised_arguments = {}
            for key, val in self.inputs.summarypages_arguments.items():
                if key == "nsamples_for_skymap":
                    self.arguments.add("nsamples_for_skymap", val)
                elif key == "gw":
                    self.arguments.add_flag("gw")
                elif key == "no_ligo_skymap":
                    self.arguments.add_flag("no_ligo_skymap")
                elif key == "burnin":
                    self.arguments.add("burnin", val)
                elif key == "kde_plot":
                    self.arguments.add_flag("kde_plot")
                elif key == "gracedb":
                    self.arguments.add("gracedb", val)
                elif key == "palette":
                    self.arguments.add("palette", val)
                elif key == "include_prior":
                    self.arguments.add_flag("include_prior")
                elif key == "notes":
                    self.arguments.add("notes", val)
                elif key == "publication":
                    self.arguments.add_flag("publication")
                elif key == "labels":
                    self.arguments.add("labels", f"{' '.join(val)}")
                else:
                    not_recognised_arguments[key] = val
            if not_recognised_arguments != {}:
                logger.warn(
                    "Did not recognise the summarypages_arguments {}. To find "
                    "the full list of available arguments, please run "
                    "summarypages --help".format(not_recognised_arguments)
                )

        self.process_node()
        for merged_node in merged_node_list:
            self.job.add_parent(merged_node.job)

    @property
    def executable(self):
        return self._get_executable_path("summarypages")

    @property
    def request_memory(self):
        return "16 GB"

    @property
    def log_directory(self):
        return self.inputs.summary_log_directory

class JointCoherenceRatioComputationNode(Node):
    """
    Class to add the computation of the coherence ratio 
    to the final result as a child job of the second image
    run
    """
    def __init__(self, inputs, parent_node, config, dag):
        super().__init__(inputs)
        self.dag = dag
        self.request_cpus = 1
        self.job_name = f'{parent_node.label}_joint_pe_clu_computation'
        self.label = parent_node.label
        self.result_file = parent_node.result_file
        self.setup_arguments(
            add_ini=False, add_unknown_args=False, add_command_line_args=False
        )

        self.arguments.add("result", parent_node.result_file)
        self.arguments.add("outdir", inputs.final_result_directory)

        # add the unlensed files
        self.arguments.add("unlensed-file-image-1", config.get('golum_settings', 'unlensed-file-image-1', fallback = None))
        self.arguments.add("unlensed-file-image-2", config.get('golum_settings', 'unlensed-file-image-2'))
        
        self.process_node()
        self.job.add_parent(parent_node.job)

    @property
    def executable(self):
        return self._get_executable_path('golum_joint_pe_compute_clu')

    @property
    def request_memory(self):
        return "8 GB"

    @property
    def log_directory(self):
        return self.inputs.data_analysis_log_directory

class JointBayesFactorNode(Node):
    """
    Node adapted to setup the final computation of the Bayes factor
    """

    def __init__(self, inputs, parent_node, ini_file, config, dag):

        super().__init__(inputs)

        self.dag = dag
        self.request_cpus = config.getint('population_settings', 'pop-request-cpus', fallback = 16)
        self.memory = config.getint('population_settings', 'pop-request-memory', fallback = 8)
        self.request_disk = "%i GB"%config.getint('population_settings', 'pop-request-disk', fallback = 6)
        self.job_name = f'{parent_node.label}_joint_pe_selection_effects'
        self.setup_arguments(add_ini = False, add_unknown_args=False, add_command_line_args=False)

        self.arguments.add("result", parent_node.result_file)
        self.arguments.add("outdir", inputs.final_result_directory)
        self.arguments.add("ini-file", ini_file)
        self.arguments.add("case", 'joint')
        self.label = parent_node.label
        self.result_file = parent_node.result_file
        
        self.process_node()
        self.job.add_parent(parent_node.job)


    @property
    def executable(self):
        return self._get_executable_path('golum_compute_bayes_factor')

    @property
    def request_memory(self):
        return f"{self.memory} GB"

    @property
    def log_directory(self):
        return self.inputs.data_analysis_log_directory

class GolumBayesFactorNode(Node):
    """
    Node to set up the post processing step where we 
    compute the Bayes factor value for a Golum run
    """

    def __init__(self, inputs, parent_node, ini_file, config, dag):
        super().__init__(inputs)

        self.dag = dag
        self.request_cpus = config.getint('population_settings', 'pop-request-cpus')
        self.memory = config.getint('population_settings', 'pop-request-memory', fallback = 8)
        self.request_disk = "%i GB"%config.getint('population_settings', 'pop-request-disk', fallback = 6)
        self.job_name = f'{parent_node.label}_golum_selection_effects'
        self.setup_arguments(add_ini = False, add_unknown_args=False, add_command_line_args=False)

        self.arguments.add("result", parent_node.result_file)
        self.arguments.add("outdir", inputs.final_result_directory)
        self.arguments.add("ini-file", ini_file)
        self.arguments.add("case", 'golum')
        self.label = parent_node.label
        self.result_file = parent_node.result_file
        
        self.process_node()
        self.job.add_parent(parent_node.job)


    @property
    def executable(self):
        return self._get_executable_path('golum_compute_bayes_factor')

    @property
    def request_memory(self):
        return f"{self.memory} GB"

    @property
    def log_directory(self):
        return self.inputs.data_analysis_log_directory
