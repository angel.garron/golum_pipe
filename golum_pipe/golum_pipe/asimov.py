"""
Script to perform the Asimov integration 
for golum_pipe
"""

import configparser
import json
import glob
import os
import re
import subprocess
import time
import git.exc
from liquid import Liquid

from asimov import config, logger
from asimov.pipeline import Pipeline, PipelineException, PipelineLogger#, PESummaryPipeline
from asimov.utils import update, set_directory

from .core import utils as golum_pipe_utils

try:
    from lensing_laplace.asimov import LensingLaplace
    from lensing_laplace.utils import check_railing_from_file
    laplace_installed = True
except ImportError:
    print("Post-processing jobs will not be run because laplace is not installed")
    laplace_installed = False

EXTENSIONS = ["json", "hdf5", "h5", "pickle", "pkl"]

GOLUM_DIRECTORY = os.path.dirname(os.path.abspath(__file__))


class GolumImage1(Pipeline):
    """
    Class to implement the first image golum analysis
    into Asimov.

    This is based on the same config performed for Bilby
    """

    name = "golum_image_1"
    status = {"wait", "stuck", "stopped", "running", "finished", "uploaded"}

    def __init__(self, production, category = None):
        """
        Initialization of the class. 
        """
        super(GolumImage1, self).__init__(production, category)
        self.logger.info("Using golum_pipe for the first image analysis")

        if not production.pipeline.lower() == "golum_image_1":
            raise PipelineException

    def detect_completion(self):
        """
        Asses if the bilby-based run has completed. 


        RETURNS:
        --------
        - True if the job is completed. Else, False
        """

        self.logger.info("Checking if the PE run is done")
        result_dir = glob.glob(f"{self.production.rundir}/result")
      
        if len(result_dir) > 0:
            """
            for ext in EXTENSIONS:
                result_file = glob.glob(os.path.join(result_dir[0], f"*_result.{ext}"))
            """
            result_file = glob.glob(os.path.join(result_dir[0], "*merge_result.hdf5"))
            result_file += glob.glob(os.path.join(result_dir[0], "*merge_result.json"))
            self.logger.debug(f"Result files: {result_file}")

            if len(result_file) > 0:
                self.logger.info("Result file found, job completed")
                return True

            else:
                # add an extra check for when there is no parallel runs done
                final_result_dir = glob.glob(f"{self.production.rundir}/final_result")
                final_result_file = glob.glob(os.path.join(final_result_dir[0], "*_result.hdf5"))
                final_result_file += glob.glob(os.path.join(final_result_dir[0], "*_result.json"))
                if len(final_result_file) > 0:
                    self.logger.info("Final result file found, job completed")
                    return True
                else:
                    self.logger.info("No result file found")
                    return False
        else:
            self.logger.info("No result directory found")
            return False

    def before_submit(self):
        """
        Pre-submit hook verifying whether the 
        setup for the different sub files is fine
        """
        self.logger.info("Running pre-submit hook")
        sub_files= glob.glob(f"{self.production.rundir}/submit/*.submit")
        for sub_file in sub_files:
            if "dag" in sub_file:
                continue
            with open(sub_file, 'r') as f_handle:
                original = f_handle.read()
            with open(sub_file, 'w') as f_handle:
                self.logger.info(f"Adding preserve relative path to {sub_file}")
                f_handle.write("preserve_relative_paths = True\n" + original)

    def _determine_prior(self):
        """
        Function checking if a prior file is given and
        if not setting up the correct prior file
        """

        self.logger.info("Determining production prior file")

        if 'prior file' in self.production.meta:
            self.logger.info("A prior file has already been specified")
            self.logger.info(f"{self.production.meta['prior file']}")
            return self.production.meta['prior file']

        template = None

        if "event type" in self.production.meta:
            event_type = self.production.meta['event type'].lower()
        else:
            event_type = "bbh"
            self.production.meta['event type'] = event_type

        
        if template is None:
            # look into the golum default templates
            self.logger.info(f"Checking for default template in {GOLUM_DIRECTORY}")
            template = f"{GOLUM_DIRECTORY}/asimov_templates/image_1.prior.template"
        

        priors = {}
        priors = update(priors, self.production.event.ledger.data["priors"])
        priors = update(priors, self.production.event.meta["priors"])
        priors = update(priors, self.production.meta["priors"])

        # read template with liquid
        liq = Liquid(template)
        rendered = liq.render(priors = priors, config = config)

        prior_name = f"{self.production.name}.prior"
        prior_file = os.path.join(os.getcwd(), prior_name)
        self.logger.info(f"Saving the new prior file as {prior_file}")
        with open(prior_file, 'w') as new_prior:
            new_prior.write(rendered)

        repo = self.production.event.repository
        try:
            repo.add_file(prior_file,
                          os.path.join(config.get("general", "calibration_directory"),
                                       prior_name))
            os.remove(prior_file)
        except git.exc.GitCommandError:
            pass

        return os.path.join(self.production.event.repository.directory,
                            config.get("general", "calibration_directory"),
                            prior_name)

    def build_dag(self, psds = None, user = None, dryrun = False):
        """
        Function to generate the golum dag to submit

        ARGUMENTS:
        ----------
        - psds: dictionary with the psds that should be
                provided to the dag file
        - user: user accounting tag to start the run
        - dryrun: If true, the commands will not be run but 
                 are printed
        """

        cwd = os.getcwd()
        self.logger.info(f"Working in {cwd}")
        self._determine_prior()

        if self.production.event.repository:
            ini = self.production.event.repository.find_prods(self.production.name,
                                                              self.category)[0]
            ini = os.path.join(cwd, ini)
        else:
            ini = f'{self.production.name}.ini'

        if self.production.rundir:
            rundir = self.production.rundir
        else:
            rundir = os.path.join(os.path.expanduser("~"),
                                  self.production.event.name,
                                  self.production.name)
            self.production.rundir = rundir

        if "job label" in self.production.meta:
            job_label = self.production.meta["job label"]
        else:
            job_label = self.production.name

        command = [os.path.join(config.get('pipelines', 'environment'),
                                'bin', 'golum_first_image'), 
                   ini]

        if dryrun:
            print(" ".join(command))
        else:
            # start the run
            self.logger.info(" ".join(command))
            pipe = subprocess.Popen(command,
                                    stdout = subprocess.PIPE,
                                    stderr = subprocess.STDOUT)
            out, err = pipe.communicate()
            self.logger.info(out)

            if err or "DAG generation complete, to submit jobs" not in str(out):
                self.production.status = "stuck"
                self.logger.error(err)

                raise PipelineException(f"Dag file could not be created. \n{command}\n{out}\n{err}",
                                        production = self.production.name)

            else:
                time.sleep(10)
                return PipelineLogger(message = out, production = self.production.name)

    def submit_dag(self, dryrun = False):
        """
        Command to submit the dag file to the condor cluster

        ARGUMENTS:
        ----------
        - dryrun: if True, the commands are printed rather than
                 executed
        """

        cwd = os.getcwd()
        self.logger.info(f"Working in {cwd}")
        self.before_submit()

        try:
            if "job label" in self.production.meta:
                job_label = self.production.meta['job label']
            else:
                job_label = self.production.name

            dag_filename = f"golum_image1_{job_label}.dag"

            command = ["condor_submit_dag", "-batch-name",
                       f"golumimage1/{self.production.event.name}/{self.production.name}",
                       os.path.join(self.production.rundir, "submit", dag_filename)]

            if dryrun:
                # just print the command
                print(" ".join(command))
            else:
                # actually submit the job
                self.logger.info(f"Working in {os.getcwd()}")

                dagman = subprocess.Popen(command,
                                          stdout = subprocess.PIPE,
                                          stderr = subprocess.STDOUT)
                self.logger.info(" ".join(command))
                stdout, stderr = dagman.communicate()

                if "submitted to cluster" in str(stdout):
                    cluster = re.search(r"submitted to cluster ([\d]+)",
                                        str(stdout)).groups()[0]
                    self.logger.info(f"Submitted successfully. Running with job id {int(cluster)}")
                    self.production.status = "running"
                    self.production.job_id = int(cluster)

                    return cluster, PipelineLogger(stdout)
                else:
                    self.logger.error("Could not submit the job to the cluster")
                    self.logger.info(stdout)
                    self.logger.error(stderr)

                    raise PipelineException("The dag file could not be submitted")

        except FileNotFoundError as error:
            
            self.logger.exception(error)

            raise PipelineException("It looks like condor is not installed on this system.\n"
                                    f"I wanted to run {' '.join(command)}") from error

    def collect_assets(self):
        """
        Function to collect the result assets, seen as 
        the the samples produced by the nested sampling run
        """
        
        return {'samples' : self.samples()}

    def samples(self, absolute = True):
        """
        Function to collect the combined samples file
        for PESummary

        ARGUMENTS:
        ----------
        - absolute: whether to return the absolute or relative
                    file path
        """

        if absolute:
            rundir = os.path.abspath(self.production.rundir)
        else:
            rundir = self.production.rundir
        self.logger.info(f"Run directory for samples: {rundir}")

        sample_files = glob.glob(os.path.join(rundir, "result", "*_merge*_result.hdf5"))\
                       + glob.glob(os.path.join(rundir, "result", "*_merge*_result.json"))

        return sample_files

    @classmethod
    def read_ini(cls, ini_file):
        """
        Function to read the ini file and
        return a parser

        ARGUMENTS:
        ----------
        - ini_file: the path to the ini file


        RETURNS:
        --------
        - golum_config: the config made from the ini file
        """

        golum_config = golum_pipe_utils.parse_ini_file(ini_file)

        return golum_config


    def check_type_II_bayes_factor(self, absolute = False):
        """
        Function detecting whether the type II Bayes factor
        has been computed properly if requested
        """
        # determine what the ini file
        cwd = os.getcwd()
        if self.production.event.repository:
            ini = self.production.event.repository.find_prods(self.production.name,
                                                              self.category)[0]
            ini = os.path.join(cwd, ini)
        else:
            ini = f'{self.production.name}.ini'

        # read in the ini file
        self.golum_config = self.read_ini(ini)

        type_II = self.golum_config.getboolean("type_II_search", "bayes-factor-type-2", fallback = False)

        if type_II:
            # need to check in the final result 
            # directory whether the file is there
            if absolute:
                rundir = os.path.abspath(self.production.rundir)
            else:
                rundir = self.production.rundir

            self.res_files = glob.glob(os.path.join(rundir, "final_result", "*_type_II_bayes_factors.json"))
        
            if len(self.res_files) > 0:
                self.logger.info("Type II Bayes factor computation worked")
                return True
            else:
                self.logger.info("Type II Bayes factor computation requested but not done")
                return False
        else:
            self.logger.info("Type II Bayes factor computation not requested")
            return True

    def laplace_processing_asked(self):
        """
        Function to check if laplace processing is asked
        """
        asked = False
        if "postprocessing" in self.production.meta.keys():
            if "lensinglaplace" in self.production.meta["postprocessing"].keys():
                asked = True
        return asked

    def after_completion(self, absolute = False):
        """
        Post pipeline completion hook.
        This verifies if we want to check the posteriors for
        railing or not. Then, it check whether the run prefers
        type II images or not.
        """
        super().after_completion()
        self.logger.info("Job has completed. Doing post-processing")
        # check if postprocessing has been requested
        if self.laplace_processing_asked() and not laplace_installed:
            raise ImportError("Laplace is not installed but posterior check was requested")

        if laplace_installed and self.laplace_processing_asked():
            self.logger.info("Verifying the posteriors for railing")
            post_pipeline = LensingLaplace(production = self.production)
            cluster = post_pipeline.submit_dag()
            self.production.meta["job id"] = int(cluster)
            self.production.status = "processing"
            self.production.event.update_data()
        else:
            # still put the status to processing so that ewe can start the type II 
            # image check
            self.production.status = "processing"
            self.production.event.update_data()

    def verify_if_laplace_done(self):
        """
        Function to verify if the laplace check is completed
        """
        # check the rundir
        if self.production.rundir:
            rundir = self.production.rundir
        else:
            rundir = os.path.join(os.path.expanduser("~"),
                                  self.production.event.name,
                                  self.production.name)
        laplace_files = glob.glob(os.path.join(rundir, "railing_check_*.json"))

        if len(laplace_files) > 0:
            self.logger.info("Laplace check completed")
            return True
        else:
            self.logger.info("Laplace check not completed")
            return False
    
    def detect_completion_processing(self):
        """
        Function to verify if the processing is completed
        It is the case if laplace check has been asked and is done
        but also if we have not asked for it at all
        """
        if self.laplace_processing_asked():
            laplace_done = self.verify_if_laplace_done()
            return laplace_done
        else:
            return True
        
    def after_processing(self):
        """
        Function to be run after processing,
        either when laplace is done or when the type II
        image check still needs to be done
        """

        # if asked for, see if laplace has found railing
        railing_text = "Laplace railing check was not asked for"
        if self.laplace_processing_asked():
            if self.production.rundir:
                rundir = self.production.rundir
            else:
                rundir = os.path.join(os.path.expanduser("~"),
                                      self.production.event.name,
                                      self.production.name)
            laplace_file = glob.glob(os.path.join(rundir, "railing_check_*.json"))[0]
            with open(laplace_file, 'r') as laplace_f:
                laplace_dict = json.load(laplace_f)
            if laplace_dict['railing']:
                railing_text = "railing WAS found"
                self.logger.info(f"{railing_text}")
            else:
                railing_text = "railing WAS NOT found"
                self.logger.info(f"{railing_text}")
        
        # do the type II image reading
        check_typeII = self.check_type_II_bayes_factor()
        send_email = self.golum_config.getboolean("output_settings", "email-alert", fallback = False)
        receivers = list(self.golum_config.get("output_settings", "receivers", fallback = "").strip().split(","))
        label = self.golum_config.get("output_settings", "label")
        if check_typeII:
            self.production.status =  "uploaded"
            if len(self.res_files) == 1:
                with open(self.res_files[0], "r") as fin_typeII:
                    data = json.load(fin_typeII)
                if data['logB_IIvsI'] > 0.2 and data['logB_IIvsIII'] > 0.2:
                    self.production.meta["InterestStatus"] = True
                    if send_email:
                        subject = f"[Golum type II] Interesting results for run {label} production {self.production.name} available"
                        text = f"""The golum type II analysis has found an interesting result for event {label} in production {self.production.name}\n
                                    The log Bayes factor for type II vs type I is {data['logB_IIvsI']} \n
                                    and the log Bayes factor for type II vs type III is {data['logB_IIvsIII']}. \n
                                    You should take a look at the results.\n
                                    Laplace result: {railing_text}"""
                        golum_pipe_utils.send_email_alert(subject, text, "justin.janquart@ligo.org", receivers)

                else:
                    self.production.meta["InterestStatus"] = False
                    if send_email:
                        subject = f"[Golum type II] No interesting results for run {label} production {self.production.name} available"
                        text = f"""The golum type II analysis has no interesting result for event {label} in production {self.production.name}\n
                                    The log Bayes factor for type II vs type I is {data['logB_IIvsI']} \n
                                    and the log Bayes factor for type II vs type III is {data['logB_IIvsIII']}.\n
                                    Laplace result: {railing_text}"""
                        golum_pipe_utils.send_email_alert(subject, text, "justin.janquart@ligo.org", receivers)

            else:
                self.production.meta["InterestStatus"] = None
                for file in self.res_files:
                    with open(file, "r") as fin:
                        data = json.load(fin)
                    if data['logB_IIvsI'] > 0.2 and data['logB_IIvsIII'] > 0.2:
                        self.production.meta["InterestStatus"] = True
                        if send_email:
                            subject = f"[Golum type II] Interesting results for run {label} production {self.production.name} available"
                            text = f"""The golum type II analysis has found an interesting result for event {label} in production {self.production.name}\n
                                        The log Bayes factor for type II vs type I is {data['logB_IIvsI']} \n
                                        and the log Bayes factor for type II vs type III is {data['logB_IIvsIII']}. \n
                                        You should take a look at the results.\n
                                        Laplace result: {railing_text}"""
                            golum_pipe_utils.send_email_alert(subject, text, "justin.janquart@ligo.org", receivers)

                if self.production.meta["InterestStatus"] == None:
                    self.production.meta["InterestStatus"] = False
                    if send_email:
                        subject = f"[Golum type II] No interesting results for run {label} production {self.production.name} available"
                        text = f"""The golum type II analysis has no interesting result for event {label} in production {self.production.name}\n
                                    The log Bayes factor for type II vs type I is {data['logB_IIvsI']} \n
                                    and the log Bayes factor for type II vs type III is {data['logB_IIvsIII']} \n
                                    Laplace result: {railing_text}"""
                        golum_pipe_utils.send_email_alert(subject, text, "justin.janquart@ligo.org", receivers)

            self.production.event.update_data()
        else:
            self.production.status = "stuck"
            if send_email:
                subject = f"[Golum type II] Problem with run {label} production {self.production.name} available"
                text = f"""The golum type II analysis for event {label} in production {self.production.name} is stuck\n
                            You should go and take a look.\n
                            Laplace result: {railing_text}"""
                golum_pipe_utils.send_email_alert(subject, text, "justin.janquart@ligo.org", receivers)
            self.production.event.update_data()
        
    def collect_logs(self):
        """
        Collect all the produced log files and 
        make them into a dict

        RETURNS:
        --------
        - messages: the dict with the message from the different log files
        """
        logs = glob.glob(f"{self.production.rundir}/submit/*.err")\
               + glob.glob(f"{self.production.rundir}/log*/*.err")\
               + glob.glob(f"{self.production.rundir}/*/*.out")\
               + glob.glob(f"{self.production.rundir}/*.log")

        messages = {}

        for log in logs:
            try:
                with open(log, 'r') as logf:
                    message = logf.read()
                    message = message.split("\n")
                    messages[log.split('/')[-1]] = "\n".join(message[-100:])
            except FileNotFoundError:
                messages[log.split("/")[-1]] = "There was an issue when opening this log file"


        return messages

    def check_progress(self):
        """
        Function checking the job progress.

        RETURNS:
        --------
        - Dictionary summarizing the job progress.
        """

        logs = glob.glob(f"{self.production.rundir}/log_data_analysis/*.out")

        message = {}
        for log in logs:
            try:
                with open(log, 'r') as logf:
                    message = logf.read()
                    message = message.split("\n")[-1]
                    pat = re.compile(r"([\d]+)it")
                    iterations = pat.search(message)
                    pat = re.compile(r"dlogz:([\d]*\.[d]*)")
                    dlogz = pat.search(message)
                    if iterations:
                        messages[log.split("/")[-1]] = (iterations.group(),
                                                        dlogz.group())

            except FileNotFoundError:
                messages[log.split("/")[-1]] = "The was an issue when opening this log file"

        return messages

    def html(self):
        """
        Could give the html representation of this pipeline.
        Not implemented, returning empty string
        """
        out = ""
        return out

    def resurrect(self):
        """
        Attempt to resurrect a failed job. This is tried only 
        five times when the rescue dag is produced.
        """
        try:
            count = self.production.meta['resurrections']
        except KeyError:
            count= 0

        if count < 5 and len(glob.glob(os.path.join(self.production.rundir, "submit", "*.rescue"))) > 0:
            count += 1
            self.submit_dag()


class GolumJointPE(Pipeline):
    """
    Class to implement the joint parameter estimation 
    analysis in GOLUM 

    This is based on the config for bilby 
    """

    name = "golum_joint"
    status = {"wait", "stuck", "stopped", "running", "finished", "uploaded"}

    def __init__(self, production, category = None):
        """
        Initialization of the class
        """

        self.production = production
        self.category = category
        self.logger = logger.getChild("project analysis")
        self.logger.info("Using golum_pipe for the joint parameter estimation")
        
        if not production.pipeline.lower() == "golum_joint":
            raise PipelineException(message = "Pipeline not found")

    def detect_completion(self):
        """
        Asses if the bilby-based run has completed. 
        This only verifies the parameter estimation part of the job

        RETURNS:
        --------
        - True if the job is completed. Else, False
        """

        self.logger.info("Checking if the PE run is done")
        result_dir = glob.glob(f"{self.production.rundir}/result")
        if len(result_dir) > 0:
            result_file = glob.glob(os.path.join(result_dir[0], "*merge_result.hdf5"))
            result_file += glob.glob(os.path.join(result_dir[0], "*merge_result.json"))
            self.logger.debug(f"Result files: {result_file}")

            if len(result_file) > 0:
                self.logger.info("Result file found, job completed")
                return True

            else:
                # extra check for when no merge results
                final_result_dir = glob.glob(f"{self.production.rundir}/final_result")
                final_result_file = glob.glob(os.path.join(final_result_dir[0], "*_result.hdf5"))
                final_result_file += glob.glob(os.path.join(final_result_dir[0], "*_result.json"))
                if len(final_result_file) > 0:
                    self.logger.info("Final result file found, job completed")
                    return True
                else:
                    self.logger.info("No result file found")
                    return False
        else:
            self.logger.info("No result directory found")
            return False

    def _determine_prior(self):
        """
        Function checking whether the prior file is given and if 
        not loads the correct prior
        """

        self.logger.info("Determining the prior file")

        if 'prior file' in self.production.meta:
            self.logger.info("A prior file has already been specified")
            self.logger.info(f"{self.production.meta['prior file']}")
            return self.production.meta['prior file']

        template = None

        if "event type" in self.production.meta:
            event_type = self.production.meta['event type'].lower()
        else:
            event_type = "bbh"
            self.production.meta['event type'] = event_type

        
        if template is None:
            # look into the golum default templates
            self.logger.info(f"Checking for default template in {GOLUM_DIRECTORY}")
            template = f"{GOLUM_DIRECTORY}/asimov_templates/joint_pe.prior.template"
        
        priors = {}
        priors = update(priors, self.production.event.ledger.data["priors"])
        priors = update(priors, self.production.event.meta["priors"])
        priors = update(priors, self.production.meta["priors"])

        # read template with liquid
        liq = Liquid(template)
        rendered = liq.render(priors = priors, config = config)

        prior_name = f"{self.production.name}.prior"
        prior_file = os.path.join(os.getcwd(), prior_name)
        self.logger.info(f"Saving the new prior file as {prior_file}")
        with open(prior_file, 'w') as new_prior:
            new_prior.write(rendered)

        repo = self.production.event.repository
        try:
            repo.add_file(prior_file,
                          os.path.join(config.get("general", "calibration_directory"),
                                       prior_name))
            os.remove(prior_file)
        except git.exc.GitCommandError:
            pass

        return os.path.join(self.production.event.repository.directory,
                            config.get("general", "calibration_directory"),
                            prior_name)

    def build_dag(self, psds = None, user = None, clobber_psd = False, dryrun = None):
        """
        Function constructing the dag file for the JPE runs and then 
        submitting it

        ARGUMENTS:
        ----------
        - psds: psds to be provided to dag file (not used)
        - user: the user accounting tag
        - clobber_psd: whether a clobber psd is used (not used here)
        - dryrun: If True, the commands are printed and not run

        This function raises a PipelineException if the Dag file cannot 
        be constructed
        """

        cwd = os.getcwd()
        self.logger.info(f"Working in {cwd}")
        self._determine_prior()

        if self.production.repository:
            ini = self.production.repository.find_prods(self.production.name,
                                                              self.category)[0]
            ini = os.path.join(cwd, ini)
        else:
            ini = f"{self.production.name}.ini"

        if self.production.rundir:
            rundir = self.production.rundir
        else:
            rundir = os.path.join(os.path.expanduser("~"),
                                  self.production.event.name,
                                  self.production.name)
            self.production.rundir = rundir


        if "job label" in self.production.meta:
            job_label = self.production.meta['job label']
        else:
            job_label = self.production.name


        command = [os.path.join(config.get('pipelines', 'environment'),
                                'bin', 'golum_joint_pe'), ini]
        if dryrun:
            print(" ".join(command))
        else:
            # start the run 
            self.logger.info(" ".join(command))
            pipe = subprocess.Popen(command,
                                   stdout = subprocess.PIPE,
                                   stderr = subprocess.STDOUT)
            out, err = pipe.communicate()
            self.logger.info(out)

            if err or "DAG generation complete, to submit jobs" not in str(out):
                self.production.status = "stuck"
                self.logger.error(err)

                raise PipelineException(f"Dag file cannot be created. \n {command}\n{out}\n{err}",
                                        production = self.production.name)
            else:
                time.sleep(10)
                return PipelineLogger(message = out, production = self.production.name)

    def submit_dag(self, dryrun = False):
        """
        Function submitting the dag file to the condor cluster

        ARGUMENTS:
        ----------
        - dryrun: if True, the commands are printed rather than executed
        """

        cwd = os.getcwd()
        self.logger.info(f"Working in {cwd}")
        self.before_submit()

        try:
            if "job label" in self.production.meta:
                job_label = self.production.meta['job label']
            else:
                job_label = self.production.name

            dag_filename = f"joint_pe_golum_{job_label}.dag"

            command = ["condor_submit_dag", "-batch-name",
                       f"golumjointpe/project_analyses/{self.production.name}",
                       os.path.join(self.production.rundir, "submit", dag_filename)]

            if dryrun:
                print(" ".join(command))
            else:
                # start the job 
                dagman = subprocess.Popen(command,
                                          stdout = subprocess.PIPE,
                                          stderr = subprocess.STDOUT)
                self.logger.info(" ".join(command))
                stdout, stderr = dagman.communicate()

                if "submitted to cluster" in str(stdout):
                    cluster = re.search(r"submitted to cluster ([\d]+)",
                                        str(stdout)).groups()[0]
                    self.logger.info(f"Submitted successfully. Running with job id {int(cluster)}")
                    self.production.status = "running"
                    self.production.job_id = int(cluster)

                    return cluster, PipelineLogger(stdout)

                else:
                    self.logger.error("Could not submit the job to the cluster")
                    self.logger.info(stdout)
                    self.logger.error(stderr)

                    raise PipelineException("The dag file could not be submitted")

        except FileNotFoundError as error:
            
            self.logger.exception(error)

            raise PipelineException("It looks like condor is not installed on this system.\n"
                                    f"I wanted to run {' '.join(command)}") from error

    def collect_assets(self):
        """
        Function to collect the result assets, seen as 
        the the samples produced by the nested sampling run
        """
        
        return {'samples' : self.samples()}

    def samples(self, absolute = True):
        """
        Function to collect the combined samples file
        for PESummary

        ARGUMENTS:
        ----------
        - absolute: whether to return the absolute or relative
                    file path
        """

        if absolute:
            rundir = os.path.abspath(self.production.rundir)
        else:
            rundir = self.production.rundir
        self.logger.info(f"Run directory for samples: {rundir}")

        sample_files = glob.glob(os.path.join(rundir, "result", "*_merge*_result.hdf5"))\
                       + glob.glob(os.path.join(rundir, "result", "*_merge*_result.json"))

        return sample_files

    @classmethod
    def read_ini(cls, ini_file):
        """
        Function to read the ini file and
        return a parser

        ARGUMENTS:
        ----------
        - ini_file: the path to the ini file


        RETURNS:
        --------
        - golum_config: the config made from the ini file
        """

        golum_config = golum_pipe_utils.parse_ini_file(ini_file)

        return golum_config

    def check_selection_effects(self, absolute = False):
        """
        Function checking whether the selection effect computation 
        worked 
        """

        selection_effects = self.golum_config.getboolean("population_settings", "selection-effects", fallback = False)

        if selection_effects:
            # check completion only if the analysis was asked for
            if absolute:
                rundir = os.path.abspath(self.production.rundir)
            else:
                rundir = self.production.rundir

            self.selection_effects_res_files = glob.glob(os.path.join(rundir, "final_result", "*_population_effects.json"))
            if len(self.selection_effects_res_files) > 0:
                self.logger.info("Selection effects computation worked")
                return True
            else:
                self.logger.info("Selection effects computation requested by did not work")
                return False

        else:
            self.logger.info("Selection effects computation not requested")
            return None

    def check_coherence_ratio(self):
        """
        Function to check the coherence ratio computation
        """
        
        coherence_ratio = self.golum_config.getboolean("golum_settings", "compute-coherence-ratio", fallback = False)

        if coherence_ratio:
            rundir = self.production.rundir
            self.clu_res_file = glob.glob(os.path.join(rundir, "result", "*_log_coherence_ratio.json" ))
            if len(self.clu_res_file) > 0:
                self.logger.info("Coherence ratio computation worked")
                return True
            else:
                self.logger.info("Coherence ratio computation requested but did not work")
                return False

        else:
            self.logger.info("Coherence ratio computation was not requested")
            return None

    def check_pe(self):
        """
        Function to check if the PE was done properly
        """
        self.logger.info("Checking if the PE run is done")
        result_dir = glob.glob(f"{self.production.rundir}/result")

        if len(result_dir) > 0:
            self.pe_result_file = ""
            for file in os.listdir(result_dir[0]):
                if "merge_result" in file:
                    self.pe_result_file += file 
            
            self.logger.info(f"Result files: {self.pe_result_file}")

            if len(self.pe_result_file) > 0:
                self.logger.info("Result file found, job completed")
                return True

            else:
                self.logger.info("No result file found")
                return False
        else:
            self.logger.info("No result directory found")
            return False

    def send_failure_message(self, label, receivers, railing_text = "Laplace railing check was not asked for"):
        """
        Function sending a generic failure message when the pipeline did not
        complete its task
        """
        subject = f"[Golum joint PE] run {label} production {self.production.name} stuck"
        text = f"""The golum joint PE analysis for event {label} in production {self.production.name} is STUCK\n 
                 You should take a look.\n
                 Laplace result: {railing_text}"""
        golum_pipe_utils.send_email_alert(subject, text, f"justin.janquart@ligo.org", receivers)

    def laplace_processing_asked(self):
        """
        Funciton to cehck if the laplace processing is asked
        """
        asked = False
        if "postprocessing" in self.production.meta.keys():
            if "lensinglaplace" in self.production.meta["postprocessing"].keys():
                asked = True

        return asked

    def after_completion(self):
        """
        Post pipeline completion hook. 
        """
        super().after_completion()
        self.logger.info("Job has completed. Doing post-processing")
        self.production.status = 'processing'
        
        if self.laplace_processing_asked() and not laplace_installed:
            raise ImportError("The laplace processing was requested but laplace is not installed on this system")
        
        if laplace_installed and self.laplace_processing_asked():
            self.logger.info("Verifying the posteriors for railing")
            post_pipeline = LensingLaplace(production = self.production)
            cluster = post_pipeline.submit_dag()
            self.production.meta["job id"] = int(cluster)

    def verify_if_laplace_done(self):
        """
        Function to verify if the laplace run is completed
        """
        if self.production.rundir:
            rundir = self.production.rundir
        else:
            rundir = os.path.join(os.path.expanduser("~"),
                                  self.production.event.name,
                                  self.production.name)
        laplace_files = glob.glob(os.path.join(rundir, "railing_check_*.json"))

        if len(laplace_files) > 0:
            self.logger.info("Laplace check completed")
            return True
        else:
            self.logger.info("Laplace check not completed")
            return False
    
    def detect_completion_processing(self):
        """
        Function to check if the processing job
        is completed or not
        """
        if self.laplace_processing_asked():
            laplace_done = self.verify_if_laplace_done()
        else:
            laplace_done = True

        return laplace_done
       
    def after_processing(self):
        """
        Function to run after processing, either when 
        the laplace processing is done or just if
        it was not asked for
        """

        # if asked for, see if laplace has found railing
        railing_text = "Laplace railing check was not asked for"
        if self.laplace_processing_asked():
            if self.production.rundir:
                rundir = self.production.rundir
            else:
                rundir = os.path.join(os.path.expanduser("~"),
                                      self.production.event.name,
                                      self.production.name)
            laplace_file = glob.glob(os.path.join(rundir, "railing_check_*.json"))[0]
            with open(laplace_file, 'r') as laplace_f:
                laplace_dict = json.load(laplace_f)
            if laplace_dict['railing']:
                railing_text = "railing WAS found"
                self.logger.info(f"{railing_text}")
            else:
                railing_text = "railing WAS NOT found"
                self.logger.info(f"{railing_text}")

        # check the selection effects and other things if asked for
        cwd = os.getcwd()
        subj_str = "_".join([f"{self.production.subjects[i]}" for i in range(len(self.production.subjects))])
        if os.path.isfile(f"checkouts/{subj_str}/{self.production.name}.ini"):
            ini = f"checkouts/{subj_str}/{self.production.name}.ini"
        else:
            ini = f"{self.production.name}.ini"
        self.golum_config = self.read_ini(ini)

        # check emailing info
        send_email = self.golum_config.getboolean("output_settings", "email-alert", fallback = False)
        receivers = list(self.golum_config.get("output_settings", "receivers", fallback = "").strip().split(","))
        label = self.golum_config.get("output_settings", "label")
        self.production.meta["InterestStatus"] = None

        # check the selection effects part
        are_selection_effects_ok = self.check_selection_effects()
        if are_selection_effects_ok == True:
            self.production.status = "uploaded"
            with open(self.selection_effects_res_files[0], "r") as fin:
                data = json.load(fin)
            if data['log_clu_pop'] > 0 or data['log_blu'] > 0 or data['log_odds_ratio'] > 0:
                self.production.meta['InterestStatus'] = True
                if send_email:
                    subject = f"[Golum joint PE] interesting result for run {label} production {self.production.name} available"
                    text = f"""The golum joint PE analysis has found an interesting result for event {label} in production {self.production.name}\n 
                                The requested results included selection effects.\n
                                The log population reweighted coherence ratio is {data['log_clu_pop']} \n
                                The log bayes factor is {data['log_blu']} \n
                                The log odds ratio is {data['log_odds_ratio']}. \n
                                You should take a look.\n
                                Laplace result: {railing_text}"""
                    golum_pipe_utils.send_email_alert(subject, text, f"justin.janquart@ligo.org", receivers)
            else:
                self.production.meta["InterestStatus"] = False
                if send_email:
                    subject = f"[Golum joint PE] result for run {label} production {self.production.name} available"
                    text = f"""The golum joint PE analysis is done analyzing event {label} in production {self.production.name}\n 
                                The requested results included selection effects.\n
                                The log population reweighted coherence ratio is {data['log_clu_pop']} \n
                                The log bayes factor is {data['log_blu']} \n
                                The log odds ratio is {data['log_odds_ratio']}. \n
                                Laplace result: {railing_text}"""

                    golum_pipe_utils.send_email_alert(subject, text, f"justin.janquart@ligo.org", receivers)

        elif are_selection_effects_ok == None:
            is_clu_ok = self.check_coherence_ratio()
            if is_clu_ok == True:
                self.production.status = "uploaded"
                with open(self.clu_res_file[0], "r") as fin:
                    data = json.load(fin)
                if data["log_clu"] > 0:
                    self.production.meta["InterestStatus"] = True
                    if send_email:
                        subject = f"[Golum joint PE] interesting result for run {label} production {self.production.name} available"
                        text = f"""The golum joint PE analysis has found an interesting result for event {label} in production {self.production.name}\n 
                                    The requested result included only the coherence ratio.\n
                                    Its log value is {data['log_clu']}. \n
                                    You should take a look.\n
                                    Laplace result: {railing_text}"""
                        golum_pipe_utils.send_email_alert(subject, text, f"justin.janquart@ligo.org", receivers)
                else:
                    self.production.meta["InterestStatus"] = False
                    if send_email:
                        subject = f"[Golum joint PE] result for run {label} production {self.production.name} available"
                        text = f"""The golum joint PE analysis is done analyzing event {label} in production {self.production.name}\n 
                                    The requested result included only the coherence ratio.\n
                                    Its log value is {data['log_clu']}. \n
                                    Laplace result: {railing_text}"""
                        golum_pipe_utils.send_email_alert(subject, text, f"justin.janquart@ligo.org", receivers)
               
            elif is_clu_ok == None:
                is_pe_ok = self.check_pe()
                self.production.meta["InterestStatus"] = False
                if is_pe_ok:
                    self.production.status = "uploaded"
                    if send_email:
                        subject = f"[Golum joint PE] parameter estimation run for {label} production {self.production.name} available"
                        directory = os.getcwd()
                        text = f""" The golum joint PE run is done for event {label} in production {self.production.name} \n
                                    The results can be found in {directory}/{self.production.rundir}/result\n
                                    Laplace result: {railing_text}"""
                        golum_pipe_utils.send_email_alert(subject, text, "justin.janquart@ligo.org", receivers)
               
                else:
                    self.production.status = "stuck"
                    if send_email:
                        self.send_failure_message(label, receivers, railing_text = railing_text)
            else:
                self.production.status = "stuck"
                if send_email:
                    self.send_failure_message(label, receivers, railing_text = railing_text)
        else:
            self.production.status = "stuck"
            if send_email:
                self.send_failure_message(label, receivers, railing_text = railing_text)
        
    def collect_logs(self):
        """
        Collect all the produced log files and make them 
        into a dictionary

        RETURNS:
        --------
        - messages: the dict with the messages from the different log files
        """
        logs = glob.glob(f"{self.production.rundir}/submit/*.err")\
               + glob.glob(f"{self.production.rundir}/log*/*.err")\
               + glob.glob(f"{self.production.rundir}/*/*.out")\
               + glob.glob(f"{self.production.rundir}/*.log")

        messages = {}

        for log in logs:
            try:
                with open(log, 'r') as logf:
                    message = logf.read()
                    message = message.split("\n")
                    messages[log.split('/')[-1]] = "\n".join(message[-100:])
            except FileNotFoundError:
                messages[log.split("/")[-1]] = "There was an issue when opening this log file"


        return messages

    def check_progress(self):
        """
        Function checking the job progress.

        RETURNS:
        --------
        - Dictionary summarizing the job progress.
        """

        logs = glob.glob(f"{self.production.rundir}/log_data_analysis/*.out")

        message = {}
        for log in logs:
            try:
                with open(log, 'r') as logf:
                    message = logf.read()
                    message = message.split("\n")[-1]
                    pat = re.compile(r"([\d]+)it")
                    iterations = pat.search(message)
                    pat = re.compile(r"dlogz:([\d]*\.[d]*)")
                    dlogz = pat.search(message)
                    if iterations:
                        messages[log.split("/")[-1]] = (iterations.group(),
                                                        dlogz.group())

            except FileNotFoundError:
                messages[log.split("/")[-1]] = "The was an issue when opening this log file"

        return messages

class GolumImage2(Pipeline):
    """
    Class implementing the GOLUM second image run in Asimov
    """

    name = "golum_image_2"
    status = {"wait", "stuck", "stopped", "running", "finished", "uploaded"}

    def __init__(self, production, category = None):
        """
        Initialization of the class
        """

        self.production = production
        self.category = category
        self.logger = logger.getChild("project analysis")
        self.logger.info("Using golum_pipe to analyse the second image")

        if not production.pipeline.lower() == "golum_image_2": 
            raise PipelineException(message = "Pipeline not found")

    def detect_completion(self):
        """
        Asses if the bilby-based run has completed. 
        This only verifies the parameter estimation part of the job

        RETURNS:
        --------
        - True if the job is completed. Else, False
        """

        self.logger.info("Checking if the PE run is done")
        result_dir = glob.glob(f"{self.production.rundir}/result")
      
        if len(result_dir) > 0:
            """
            for ext in EXTENSIONS:
                result_file = glob.glob(os.path.join(result_dir[0], f"*_result.{ext}"))
            """
            result_file = glob.glob(os.path.join(result_dir[0], "*merge_result.hdf5"))
            result_file += glob.glob(os.path.join(result_dir[0], "*merge_result.json"))
            self.logger.debug(f"Result files: {result_file}")

            if len(result_file) > 0:
                self.logger.info("Result file found, job completed")
                return True

            else:
                # extra check for when no merge results
                final_result_dir = glob.glob(f"{self.production.rundir}/final_result")
                final_result_file = glob.glob(os.path.join(final_result_dir[0], "*_result.hdf5"))
                final_result_file += glob.glob(os.path.join(final_result_dir[0], "*_result.json"))
                if len(final_result_file) > 0:
                    self.logger.info("Final result file found, job completed")
                    return True
                else:
                    self.logger.info("No result file found")
                    return False
        else:
            self.logger.info("No result directory found")
            return False

    def _determine_prior(self):
        """
        Function checking whether the prior file is given and if 
        not loads the correct prior
        """

        self.logger.info("Determining the prior file")

        if 'prior file' in self.production.meta:
            self.logger.info("A prior file has already been specified")
            self.logger.info(f"{self.production.meta['prior file']}")
            return self.production.meta['prior file']

        template = None

        if "event type" in self.production.meta:
            event_type = self.production.meta['event type'].lower()
        else:
            event_type = "bbh"
            self.production.meta['event type'] = event_type

        
        if template is None:
            # look into the golum default templates
            self.logger.info(f"Checking for default template in {GOLUM_DIRECTORY}")
            template = f"{GOLUM_DIRECTORY}/asimov_templates/golum_second_image.prior.template"
        
        priors = {}
        priors = update(priors, self.production.event.ledger.data["priors"])
        priors = update(priors, self.production.event.meta["priors"])
        priors = update(priors, self.production.meta["priors"])

        # read template with liquid
        liq = Liquid(template)
        rendered = liq.render(priors = priors, config = config)

        prior_name = f"{self.production.name}.prior"
        prior_file = os.path.join(os.getcwd(), prior_name)
        self.logger.info(f"Saving the new prior file as {prior_file}")
        with open(prior_file, 'w') as new_prior:
            new_prior.write(rendered)

        repo = self.production.event.repository
        try:
            repo.add_file(prior_file,
                          os.path.join(config.get("general", "calibration_directory"),
                                       prior_name))
            os.remove(prior_file)
        except git.exc.GitCommandError:
            pass

        return os.path.join(self.production.event.repository.directory,
                            config.get("general", "calibration_directory"),
                            prior_name)


    def build_dag(self, psds = None, user = None, clobber_psd = False, dryrun = None):
        """
        Function constructing the dag file for the second image run and then makes
        it possible to submit it

        ARGUMENTS:
        ----------
        - psds: the psds to be provided to the dag file
        - user: the user accounting tag
        - clobber_psd: whether a clobber psd is used
        - dryrun: If true, the commands are printed and not run 

        This function raises a PipelineException error if the dag file cannot be 
        constructed
        """

        cwd = os.getcwd()
        self.logger.info(f"Working in {cwd}")
        self._determine_prior()

        if self.production.repository:
            ini = self.production.repository.find_prods(self.production.name,
                                                        self.category)[0]
            ini = os.path.join(cwd, ini)
        else:
            ini = f"{self.production.name}.ini"

        if self.production.rundir:
            rundir = self.production.rundir
        else:
            rundir = os.path.join(os.path.expanduser("~"),
                                  self.production.event.name,
                                  self.production.name)
            self.production.rundir = rundir

        if "job label" in self.production.meta:
            job_label = self.production.meta['job label']
        else:
            job_label = self.production.name

        command = [os.path.join(config.get('pipelines', 'environment'), 'bin',
                                'golum_second_image'), ini]

        if dryrun:
            print(" ".join(command))
        else:
            # start the run 
            self.logger.info(" ".join(command))
            pipe = subprocess.Popen(command,
                                   stdout = subprocess.PIPE,
                                   stderr = subprocess.STDOUT)
            out, err = pipe.communicate()
            self.logger.info(out)

            if err or "DAG generation complete, to submit jobs" not in str(out):
                self.production.status = "stuck"
                self.logger.error(err)

                raise PipelineException(f"Dag file cannot be created. \n {command}\n{out}\n{err}",
                                        production = self.production.name)
            else:
                time.sleep(10)
                return PipelineLogger(message = out, production = self.production.name)

    def submit_dag(self, dryrun = False):
        """
        Function used to submit the dag file to the cluster

        ARGUMENTS:
        ----------
        - dryrun: if True, the commands are printed rather than executed
        """

        cwd = os.getcwd()
        self.logger.info(f"Working in {cwd}")
        self.before_submit()

        try:
            if "job label" in self.production.meta:
                job_label = self.production.meta["job label"]
            else:
                job_label = self.production.name

            dag_filename = f"golum_image2_{job_label}.dag"

            command = ["condor_submit_dag", "-batch-name", 
                       f"golum_second_image/project_analyses/{self.production.name}",
                       os.path.join(self.production.rundir, "submit", dag_filename)]

            if dryrun:
                print(" ".join(command))
            else:
                # start the job 
                dagman = subprocess.Popen(command,
                                          stdout = subprocess.PIPE,
                                          stderr = subprocess.STDOUT)
                self.logger.info(" ".join(command))
                stdout, stderr = dagman.communicate()

                if "submitted to cluster" in str(stdout):
                    cluster = re.search(r"submitted to cluster ([\d]+)",
                                        str(stdout)).groups()[0]
                    self.logger.info(f"Submitted successfully. Running with job id {int(cluster)}")
                    self.production.status = "running"
                    self.production.job_id = int(cluster)

                    return cluster, PipelineLogger(stdout)

                else:
                    self.logger.error("Could not submit the job to the cluster")
                    self.logger.info(stdout)
                    self.logger.error(stderr)

        except FileNotFoundError as error:

            self.logger.exception(error)
            raise PipelineException("It looks like condor is not installed on this system.\n"
                                    f"I wanted to run {' '.join(command)}") from error

    def collect_assets(self):
        """
        Function to collect the result assests, seen as the results produced
        by the nested sampling run
        """

        return {"samples" : self.samples()}

    def samples(self, absolute = True):
        """
        Function to collect the combined samples file

        ARGUMENTS:
        ----------
        - absolute: whether to return the absolute or relative file path
        """
        if absolute:
            rundir = os.path.abspath(self.production.rundir)
        else:
            rundir = self.production.rundir
        self.logger.info(f"Run directory for samples: {rundir}")

        sample_files = glob.glob(os.path.join(rundir, "result", "*_merge*_result.hdf5"))\
                       + glob.glob(os.path.join(rundir, "result", "*_merge*_result.json"))

        return sample_files

    @classmethod
    def read_ini(cls, ini_file):
        """
        Function to read the ini file and
        return a parser

        ARGUMENTS:
        ----------
        - ini_file: the path to the ini file


        RETURNS:
        --------
        - golum_config: the config made from the ini file
        """

        golum_config = golum_pipe_utils.parse_ini_file(ini_file)

        return golum_config

    def check_selection_effects(self, absolute = False):
        """
        Function checking whether the selection effect computation 
        worked 
        """

        selection_effects = self.golum_config.getboolean("population_settings", "selection-effects", fallback = False)

        if selection_effects:
            # check completion only if the analysis was asked for
            if absolute:
                rundir = os.path.abspath(self.production.rundir)
            else:
                rundir = self.production.rundir

            self.selection_effects_res_files = glob.glob(os.path.join(rundir, "final_result", "*_population_effects.json"))
            if len(self.selection_effects_res_files) > 0:
                self.logger.info("Selection effects computation worked")
                return True
            else:
                self.logger.info("Selection effects computation requested by did not work")
                return False

        else:
            self.logger.info("Selection effects computation not requested")
            return None

    def check_coherence_ratio(self):
        """
        Function to check the coherence ratio computation
        """
        
        coherence_ratio = self.golum_config.getboolean("golum_settings", "compute-coherence-ratio", fallback = False)

        if coherence_ratio:
            rundir = self.production.rundir
            self.clu_res_file = glob.glob(os.path.join(rundir, "result", "*_log_coherence_ratio.json" ))
            if len(self.clu_res_file) > 0:
                self.logger.info("Coherence ratio computation worked")
                return True
            else:
                self.logger.info("Coherence ratio computation requested but did not work")
                return False

        else:
            self.logger.info("Coherence ratio computation was not requested")
            return None

    def check_pe(self):
        """
        Function to check if the PE was done properly
        """
        self.logger.info("Checking if the PE run is done")
        result_dir = glob.glob(f"{self.production.rundir}/result")

        if len(result_dir) > 0:
            self.pe_result_file = ""
            for file in os.listdir(result_dir[0]):
                if "merge_result" in file:
                    self.pe_result_file += file 
            
            self.logger.info(f"Result files: {self.pe_result_file}")

            if len(self.pe_result_file) > 0:
                self.logger.info("Result file found, job completed")
                return True

            else:
                self.logger.info("No result file found")
                return False
        else:
            self.logger.info("No result directory found")
            return False

    def send_failure_message(self, label, receivers):
        """
        Function sending a generic failure message when the pipeline did not
        complete its task
        """
        subject = f"[Golum image 2] run {label} production {self.production.name} stuck"
        text = f"""The golum image 2 analysis for event {label} in production {self.production.name} is STUCK\n 
                 You should take a look."""
        golum_pipe_utils.send_email_alert(subject, text, f"justin.janquart@ligo.org", receivers)

    def laplace_prcessing_asked(self):
        """
        Function to check if the laplace processing is asked
        """
        asked = False
        if "postprocessing" in self.production.meta.keys():
            if "lensinglaplace" in self.production.meta["postprocessing"].keys():
                asked = True
        
        return asked

    def after_completion(self):
        """
        Post completion hook pipeline
        """
        super().after_completion()
        self.logger.info("Job has completed. Doing post-processing")
        self.production.status = 'processing'

        if self.laplace_prcessing_asked() and not laplace_installed:
            raise ImportError("Laplace processing is requested but laplace is not installed")

        if laplace_installed and self.laplace_prcessing_asked():
            self.logger.info("Verifying posteriors for railing")
            post_pipeline = LensingLaplace(production = self.production)
            cluster = post_pipeline.submit_dag()
            self.production.meta["job id"] = int(cluster)

    def verify_if_laplace_done(self):
        """
        Function to verify if laplace run is done 
        """
        if self.production.rundir:
            rundir = self.production.rundir
        else:
            rundir = os.path.join(os.path.expanduser("~"),
                                  self.production.event.name,
                                  self.production.name)
        laplace_files = glob.glob(os.path.join(rundir, "railing_check_*.json"))
        
        if len(laplace_files) > 0:
            self.logger.info("Laplace check completed")
            return True
        else:
            self.logger.info("Laplace check not completed")
            return False
        
    def detect_completion_processing(self):
        """
        Function verifying if the processing job
        is completed
        """
        if self.laplace_prcessing_asked():
            laplace_done = self.verify_if_laplace_done()
        else:
            laplace_done = True

        return laplace_done
    
    def after_processing(self):
        """
        Function running the needed commands when processing is done
        """
        # check for railing if needed
        railing_text = "Laplace railing check was not asked for"
        if self.laplace_prcessing_asked():
            if self.production.rundir:
                rundir = self.production.rundir
            else:
                rundir = os.path.join(os.path.expanduser("~"),
                                      self.production.event.name,
                                      self.production.name)
            laplace_file = glob.glob(os.path.join(rundir, "railing_check_*.json"))[0]
            with open(laplace_file, 'r') as laplace_f:
                laplace_dict = json.load(laplace_f)
            if laplace_dict['railing']:
                railing_text = "railing WAS found"
                self.logger.info(f"{railing_text}")
            else:
                railing_text = "railing WAS NOT found"
                self.logger.info(f"{railing_text}")
        
        # check for selection effects and other things if asked for 
        cwd = os.getcwd()
        subj_str = "_".join([f"{self.production.subjects[i]}" for i in range(len(self.production.subjects))])
        if os.path.isfile(f"checkouts/{subj_str}/{self.production.name}.ini"):
            ini = f"checkouts/{subj_str}/{self.production.name}.ini"
        else:
            ini = f"{self.production.name}.ini"
        self.golum_config = self.read_ini(ini)

        # check emailing info
        send_email = self.golum_config.getboolean("output_settings", "email-alert", fallback = False)
        receivers = list(self.golum_config.get("output_settings", "receivers", fallback = "").strip().split(","))
        label = self.golum_config.get("output_settings", "label")
        self.production.meta["InterestStatus"] = None

        # check the selection effects part
        are_selection_effects_ok = self.check_selection_effects()
        if are_selection_effects_ok == True:
            self.production.status = "uploaded"
            with open(self.selection_effects_res_files[0], "r") as fin:
                data = json.load(fin)
            if data['log_clu_pop'] > 0 or data['log_blu'] > 0 or data['log_odds_ratio'] > 0:
                self.production.meta['InterestStatus'] = True
                if send_email:
                    subject = f"[Golum image 2] interesting result for run {label} production {self.production.name} available"
                    text = f"""The golum image 2 analysis has found an interesting result for event {label} in production {self.production.name}\n 
                                The requested results included selection effects.\n
                                The log population reweighted coherence ratio is {data['log_clu_pop']} \n
                                The log bayes factor is {data['log_blu']} \n
                                The log odds ratio is {data['log_odds_ratio']}. \n
                                You should take a look.\n
                                Laplace result: {railing_text}."""
                    golum_pipe_utils.send_email_alert(subject, text, f"justin.janquart@ligo.org", receivers)
            else:
                self.production.meta["InterestStatus"] = False
                if send_email:
                    subject = f"[Golum image 2] result for run {label} production {self.production.name} available"
                    text = f"""The golum image 2 analysis is done analyzing event {label} in production {self.production.name}\n 
                                The requested results included selection effects.\n
                                The log population reweighted coherence ratio is {data['log_clu_pop']} \n
                                The log bayes factor is {data['log_blu']} \n
                                The log odds ratio is {data['log_odds_ratio']}. \n
                                Laplace result: {railing_text}.
                                """
                    golum_pipe_utils.send_email_alert(subject, text, f"justin.janquart@ligo.org", receivers)

        elif are_selection_effects_ok == None:
            is_clu_ok = self.check_coherence_ratio()
            if is_clu_ok == True:
                self.production.status = "uploaded"
                with open(self.clu_res_file[0], "r") as fin:
                    data = json.load(fin)
                if data["log_clu"] > 0:
                    self.production.meta["InterestStatus"] = True
                    if send_email:
                        subject = f"[Golum image 2] interesting result for run {label} production {self.production.name} available"
                        text = f"""The golum image 2 analysis has found an interesting result for event {label} in production {self.production.name}\n 
                                    The requested result included only the coherence ratio.\n
                                    Its log value is {data['log_clu']}. \n
                                    You should take a look.\n
                                    Laplace result: {railing_text}."""
                        golum_pipe_utils.send_email_alert(subject, text, f"justin.janquart@ligo.org", receivers)
                else:
                    self.production.meta["InterestStatus"] = False
                    if send_email:
                        subject = f"[Golum image 2] result for run {label} production {self.production.name} available"
                        text = f"""The golum image 2 analysis is done analyzing event {label} in production {self.production.name}\n 
                                    The requested result included only the coherence ratio.\n
                                    Its log value is {data['log_clu']}. \n
                                    Laplace result: {railing_text}.
                                    """
                        golum_pipe_utils.send_email_alert(subject, text, f"justin.janquart@ligo.org", receivers)
               
            elif is_clu_ok == None:
                is_pe_ok = self.check_pe()
                self.production.meta["InterestStatus"] = False
                if is_pe_ok:
                    self.production.status = "uploaded"
                    if send_email:
                        subject = f"[Golum image 2] parameter estimation run for {label} production {self.production.name} available"
                        directory = os.getcwd()
                        text = f""" The golum image 2 run is done for event {label} in production {self.production.name} \n
                                    The results can be found in {directory}/{self.production.rundir}/result\n
                                    Laplace result: {railing_text}."""
                        golum_pipe_utils.send_email_alert(subject, text, "justin.janquart@ligo.org", receivers)
               
                else:
                    self.production.status = "stuck"
                    if send_email:
                        self.send_failure_message(label, receivers)
            else:
                self.production.status = "stuck"
                if send_email:
                    self.send_failure_message(label, receivers)
        else:
            self.production.status = "stuck"
            if send_email:
                self.send_failure_message(label, receivers)

    def collect_logs(self):
        """
        Collect all the produced log files and make them 
        into a dictionary

        RETURNS:
        --------
        - messages: the dict with the messages from the different log files
        """
        logs = glob.glob(f"{self.production.rundir}/submit/*.err")\
               + glob.glob(f"{self.production.rundir}/log*/*.err")\
               + glob.glob(f"{self.production.rundir}/*/*.out")\
               + glob.glob(f"{self.production.rundir}/*.log")

        messages = {}

        for log in logs:
            try:
                with open(log, 'r') as logf:
                    message = logf.read()
                    message = message.split("\n")
                    messages[log.split('/')[-1]] = "\n".join(message[-100:])
            except FileNotFoundError:
                messages[log.split("/")[-1]] = "There was an issue when opening this log file"


        return messages

    def check_progress(self):
        """
        Function checking the job progress.

        RETURNS:
        --------
        - Dictionary summarizing the job progress.
        """

        logs = glob.glob(f"{self.production.rundir}/log_data_analysis/*.out")

        message = {}
        for log in logs:
            try:
                with open(log, 'r') as logf:
                    message = logf.read()
                    message = message.split("\n")[-1]
                    pat = re.compile(r"([\d]+)it")
                    iterations = pat.search(message)
                    pat = re.compile(r"dlogz:([\d]*\.[d]*)")
                    dlogz = pat.search(message)
                    if iterations:
                        messages[log.split("/")[-1]] = (iterations.group(),
                                                        dlogz.group())

            except FileNotFoundError:
                messages[log.split("/")[-1]] = "The was an issue when opening this log file"

        return messages
