"""
Script starting the routine to compute the coherence ratio
for an event pair
"""

import re
import sys
from golum_pipe.postprocessing.pair_coherence_ratio_computation import main

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(main())