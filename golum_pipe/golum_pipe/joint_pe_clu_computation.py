"""
Script to start the coherence ratio computation after
jpe is done
"""
import re
import sys
from golum_pipe.postprocessing.joint_pe_compute_clu import main

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(main())