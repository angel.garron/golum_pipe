#!/usr/bin/env python
"""
Script to generate the second image without having prior issues when 
initiating the likelihood
"""

import glob
import os
import sys
import inspect
import itertools
import json
import shutil
import subprocess

import gwpy
import lal
import numpy as np

import bilby
from bilby.gw.detector import PowerSpectralDensity
from bilby_pipe.input import Input
from bilby_pipe.main import parse_args
from bilby_pipe.parser import create_parser
from bilby_pipe.plotting_utils import plot_whitened_data, strain_spectrogram_plot
from bilby_pipe.utils import (
    BilbyPipeError,
    DataDump,
    convert_string_to_dict,
    get_geocent_time_with_uncertainty,
    get_version_information,
    is_a_power_of_2,
    log_version_information,
    logger,
)

from bilby_pipe.data_generation import DataGenerationInput
from importlib import import_module
# fmt: off
import matplotlib  # isort:skip
matplotlib.use("agg")
# fmt: on


try:
    import nds2  # noqa
except ImportError:
    logger.warning(
        "You do not have nds2 (python-nds2-client) installed. You may "
        " experience problems accessing interferometer data."
    )

try:
    import LDAStools.frameCPP  # noqa
except ImportError:
    logger.warning(
        "You do not have LDAStools.frameCPP (python-ldas-tools-framecpp) "
        "installed. You may experience problems accessing interferometer data."
    )


class GolumImage2DataGenerationInput(DataGenerationInput):
    """
    Adapted handler for user-input to the data generation script

    PARAMETERS:
    -----------
    - args: the arguments known to bilby 
    - unknown_args: the arguments not known for bilby
    - create_data: default is True, if false, the data is not generated
    """

    def __init__(self, args, unknown_args, create_data = True):
        super().__init__(args, unknown_args, create_data)

        # add some stuff to deal with the prior
        self.known_args = args
        self.unknown_args = unknown_args
        
        self.golum_likeli_kwargs = convert_string_to_dict(args.extra_likelihood_kwargs)
        self.effective_params = self.golum_likeli_kwargs['use_effective_parameters']
            
        # need to convert the posteriors to floats
        for key in self.golum_likeli_kwargs['posteriors'].keys():
            self.golum_likeli_kwargs['posteriors'][key] = [float(self.golum_likeli_kwargs['posteriors'][key][ii]) for ii in range(len(self.golum_likeli_kwargs['posteriors'][key]))]
        
        self.t_img2 = float(args.trigger_time)

        self.t_img1 = self.get_img1_from_unknown_args()

    def get_img1_from_unknown_args(self):
        """
        Function to get the geocentric time for the first image 
        based on the unknown args.

        If it is not parsed, it is set to None. This can happen 
        when using the effective parameters.
        """
        trig_time_1 = None
        for st in self.unknown_args:
            if 'trigger-time-image-1' in st and '=' in st:
                trig_time_1 = float(st.split('=')[-1])
            elif '.' in st:
                try:
                    trig_time_1 = float(st)
                except:
                    pass

        return trig_time_1

    def create_time_delay_prior(self):
        """
        Function creating the time delay prior for the run 
        """

        # need to somehow estimate the time delay 
        if self.t_img1 is None:
            raise ValueError("Did not find a time for first image trigger")

        else:
            dt = self.t_img2 - self.t_img1
            dt_prior = bilby.core.prior.Uniform(name = 'delta_t', minimum = dt - self.deltaT,
                                                maximum = dt + self.deltaT, latex_label = r'$\Delta t$',
                                                unit = '$s$')
            return dt_prior

    def create_geocent_time_prior(self):
        """
        Function creating the geocentric time prior for the run 
        """

        geocent_time_prior = bilby.core.prior.Uniform(name = 'geocent_time', minimum = self.t_img2 - self.deltaT/2.,
                                                      maximum = self.t_img2 + self.deltaT/2.,
                                                      latex_label = r'$t_{c, 2}$', unit = '$s$')

        return geocent_time_prior

    def _get_priors(self, add_time = True):
        """
        Overwrites the function with the same name. 
        This is done to properly handle the possibility of
        having the time delay (delta_t) in Golum. This way, 
        it can be parsed and used for the lookup table
        """

        if self.default_prior in self.combined_default_prior_dicts.keys():
            prior_class = self.combined_default_prior_dicts[self.default_prior]
        elif "." in self.default_prior:
            prior_class = get_function_from_string_path(self.default_prior)
        else:
            raise ValueError("Unable to set prior: default_prior unavailable")

        if self.prior_dict is not None:
            priors = prior_class(dictionary = self.prior_dict)
        else:
            priors = prior_class(filename = self.prior_file)

        priors = self._update_default_prior_to_sky_frame_parameters(priors)

        # this part is modified to account for the time delay in the
        # golum priors

        if 'delta_t' in priors:
            logger.debug('Using the time delay prior from the prior file')
        elif 'geocent_time' in priors:
            logger.debug('Using the geocentric time prior from prior file')
        elif add_time:
            # in this case, we compute the prior for the time delay by ourselves
            if self.effective_params:
                priors['geocent_time'] = self.create_geocent_time_prior()
            else:
                priors['delta_t'] = self.create_time_delay_prior()
            
        else:
            logger.debug("No time prior available or requested")
            priors = None

        if self.calibration_model is not None:
            priors.update(self.calibration_prior)

        return priors

    @property
    def likelihood(self):
        self.search_priors = self._get_priors() # add the time delay prior
        likelihood_kwargs = dict(interferometers=self.interferometers,
            waveform_generator=self.waveform_generator,
            priors=self.search_priors,
            phase_marginalization=self.phase_marginalization,
            distance_marginalization=self.distance_marginalization,
            distance_marginalization_lookup_table=self.distance_marginalization_lookup_table,
            time_marginalization=self.time_marginalization,
            reference_frame=self.reference_frame,
            time_reference=self.time_reference,
        )

        if getattr(self, "likelihood_lookup_table", None) is not None:
            logger.info("Using internally loaded likelihood_lookup_table")
            likelihood_kwargs["distance_marginalization_lookup_table"] = getattr(
                self, "likelihood_lookup_table"
            )

        if self.likelihood_type in ["GravitationalWaveTransient", "zero"]:
            Likelihood = bilby.gw.likelihood.GravitationalWaveTransient
            likelihood_kwargs.update(jitter_time=self.jitter_time)

        elif self.likelihood_type == "ROQGravitationalWaveTransient":
            Likelihood = bilby.gw.likelihood.ROQGravitationalWaveTransient
            likelihood_kwargs.update(
                self.roq_likelihood_kwargs, jitter_time=self.jitter_time
            )
        elif "." in self.likelihood_type:
            split_path = self.likelihood_type.split(".")
            module = ".".join(split_path[:-1])
            likelihood_class = split_path[-1]
            Likelihood = getattr(import_module(module), likelihood_class)
            if 'seed' in self.extra_likelihood_kwargs.keys():
                try:
                    if self.extra_likelihood_kwargs.keys() == 'None':
                        self.extra_likelihood_kwargs['seed'] = None
                    else:
                        self.extra_likelihood_kwargs['seed'] = float(self.extra_likelihood_kwargs['seed'])
                except:
                    self.extra_likelihood_kwargs['seed'] = None
            likelihood_kwargs.update(self.extra_likelihood_kwargs)
            # need to convert the posteriors to floats in the proper way
            if 'posteriors' in likelihood_kwargs.keys():
                likelihood_kwargs['posteriors'] = self.golum_likeli_kwargs['posteriors'].copy()
            if "roq" in self.likelihood_type.lower():
                likelihood_kwargs.update(self.roq_likelihood_kwargs)
        else:
            raise ValueError("Unknown Likelihood class {}")

        likelihood_kwargs = {
            key: likelihood_kwargs[key]
            for key in likelihood_kwargs
            if key in inspect.getfullargspec(Likelihood.__init__).args
        }

        logger.debug(
            f"Initialise likelihood {Likelihood} with kwargs: \n{likelihood_kwargs}"
        )
        
        likelihood = Likelihood(**likelihood_kwargs)

        # If requested, use a zero likelihood: for testing purposes
        if self.likelihood_type == "zero":
            logger.info("Using a ZeroLikelihood")
            likelihood = bilby.core.likelihood.ZeroLikelihood(likelihood)

        return likelihood

def create_generation_parser():

    return create_parser(top_level = False)

def main():
    args, unknown_args = parse_args(sys.argv[1:], create_generation_parser())
    log_version_information()
    data = GolumImage2DataGenerationInput(args, unknown_args)
    data.save_data_dump()
    logger.info("Completed data generation")
