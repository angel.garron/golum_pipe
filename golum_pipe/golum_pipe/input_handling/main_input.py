"""
File modifying the way in which the input is done in bilby_pipe
as to properly account for the priors
"""

import bilby
from bilby_pipe.main import MainInput
from bilby_pipe.utils import get_function_from_string_path, logger, convert_string_to_dict

class GolumImage2MainInput(MainInput):
    """
    Class taking over the MainInput bilby_pipe class to modify 
    the way in which the prior is handled. This way, we should be 
    able to parse the time delay as a prior
    """

    def __init__(self, args, unknown_args):
        super().__init__(args, unknown_args)
        self.known_args = args
        self.unknown_args = unknown_args
        self.golum_likeli_kwargs = convert_string_to_dict(args.extra_likelihood_kwargs)

        self.effective_params = self.golum_likeli_kwargs['use_effective_parameters']
            
        self.t_img2 = float(args.trigger_time)

        self.t_img1 = self.get_img1_from_unknown_args()

    def get_img1_from_unknown_args(self):
        """
        Function to get the geocentric time for the first image 
        based on the unknown args.

        If it is not parsed, it is set to None. This can happen 
        when using the effective parameters.
        """
        trig_time_1 = None
        for st in self.unknown_args:
            if 'trigger-time-image-1' in st:
                trig_time_1 = float(st.split('=')[-1])

        return trig_time_1

    def create_time_delay_prior(self):
        """
        Function creating the time delay prior for the run 
        """

        # need to somehow estimate the time delay 
        if self.t_img1 is None:
            raise ValueError("Did not find a time for first image trigger")

        else:
            dt = self.t_img2 - self.t_img1
            dt_prior = bilby.core.prior.Uniform(name = 'delta_t', minimum = dt - self.deltaT,
                                                maximum = dt + self.deltaT, latex_label = r'$\Delta t$',
                                                unit = '$s$')
            return dt_prior

    def create_geocent_time_prior(self):
        """
        Function creating the geocentric time prior for the run 
        """

        geocent_time_prior = bilby.core.prior.Uniform(name = 'geocent_time', minimum = self.t_img2 - self.deltaT/2.,
                                                      maximum = self.t_img2 + self.deltaT/2.,
                                                      latex_label = r'$t_{c, 2}$', unit = '$s$')

        return geocent_time_prior

    def _get_priors(self, add_time = True):
        """
        Overwrites the function with the same name. 
        This is done to properly handle the possibility of
        having the time delay (delta_t) in Golum. This way, 
        it can be parsed and used for the lookup table
        """

        if self.default_prior in self.combined_default_prior_dicts.keys():
            prior_class = self.combined_default_prior_dicts[self.default_prior]
        elif "." in self.default_prior:
            prior_class = get_function_from_string_path(self.default_prior)
        else:
            raise ValueError("Unable to set prior: default_prior unavailable")

        if self.prior_dict is not None:
            priors = prior_class(dictionary = self.prior_dict)
        else:
            priors = prior_class(filename = self.prior_file)

        priors = self._update_default_prior_to_sky_frame_parameters(priors)

        # this part is modified to account for the time delay in the
        # golum priors

        if 'delta_t' in priors:
            logger.debug('Using the time delay prior from the prior file')
        elif 'geocent_time' in priors:
            logger.debug('Using the geocentric time prior from prior file')
        elif add_time:
            # in this case, we compute the prior for the time delay by ourselves
            if self.effective_params:
                priors['geocent_time'] = self.create_geocent_time_prior()
            else:
                priors['delta_t'] = self.create_time_delay_prior()
            
        else:
            logger.debug("No time prior available or requested")

        if self.calibration_model is not None:
            priors.update(self.calibration_prior)

        return priors
