"""
Script taking care of starting the second image run.
Reads the .ini file and test the rest using bilby_pipe
"""

import argparse
import bilby 
import bilby_pipe
import subprocess
import shlex

from golum_pipe.core import utils, golum_parsers, to_bilby_pipe
from golum_pipe.core.utils import golum_logger_img2
from golum_pipe.input_handling import main_input
from golum_pipe.job_generation import dag_generation

def main():
    """
    Main function reading the command line and the
    configuration ini file. It will start the job 
    using bilby pipe.
    """

    # start by parsing the arguments
    golumparser = golum_parsers.set_golum_parser()
    args = golumparser.parse_args()

    # read in the ini and check it for sanity 
    config = utils.verify_config_file(args)

    # verifiy if it is an injection and whether the run 
    # should be local
    if config.getboolean('injection_settings', 'injection'):
        args.injection = True
    if config.getboolean('condor_settings', 'local'):
        args.local = True

    # setup the output directories
    outdir = config.get('output_settings' ,'outdir', fallback = ".")
    output_directories = {'outdir' : outdir, 'data' : f'{outdir}/data',
                          'submit' : f'{outdir}/submit'}

    for _, directory in output_directories.items():
        bilby.core.utils.check_directory_exists_and_if_not_mkdir(directory)


    # setup the logger
    logging_level = config.get('output_settings', 'logging-level', fallback = 'INFO')
    utils.setup_logger(output_directories['outdir'], logging_level, args = args)
    golum_logger_img2.info(f'Golum logger is setup, log output in {outdir}/golum_img2.log')

    # check if the analysis is an injection, and if its, make it
    if args.injection or config.getboolean('injection_settings', 'injection'):
        golum_logger_img2.info('Generating injection information')
        injection_waveform_arguments = utils.make_waveform_arguments(config, injection = True)
        golum_logger_img2.info('Injection waveform arguments: %s', injection_waveform_arguments)
        injection_file = utils.make_injection_file(config, img2 = True)
        golum_logger_img2.info('Injection file generated')
    else:
        injection_file = None
        injection_waveform_arguments = None

    # read in the information analysis
    golum_logger_img2.info('Generating analysis waveform arguments')
    analysis_waveform_arguments = utils.make_waveform_arguments(config)
    golum_logger_img2.info('Analysis waveform arguments: %s', analysis_waveform_arguments)

    # setup the various arguments as to get to the 
    # bilby_pipe file
    bilby_pipe_config = to_bilby_pipe.image2_args_to_bilby_pipe_config(config, 
                                       args, output_directories,
                                       injection_file = injection_file,
                                       injection_waveform_arguments = injection_waveform_arguments,
                                       analysis_waveform_arguments = analysis_waveform_arguments)
    
    bilby_pipe_args, bilby_pipe_unknown_args, bilby_pipe_parser = to_bilby_pipe.get_bilby_pipe_arguments(bilby_pipe_config)
    bilby_pipe_input = main_input.GolumImage2MainInput(bilby_pipe_args, bilby_pipe_unknown_args)
    bilby_pipe_input.pretty_print_prior()
    bilby_pipe.main.write_complete_config_file(bilby_pipe_parser, bilby_pipe_args, bilby_pipe_input)
    dag_generation.generate_dag_second_image(bilby_pipe_input, args.ini, config)

    golum_logger_img2.info('Bilby pipe file generation DONE')

    # make the final dag if needed
    if not args.local or config.getboolean('run_settings', 'local', fallback = False):
        golum_logger_img2.info('Constructing final dag')
        final_dag = utils.make_final_dag(config, output_directories, img = 2)
        if config.getboolean('condor_settings', 'submit', fallback = False):
            golum_logger_img2.info(f"Submitting dag {final_dag} to condor")
            
            command = f'condor_submit_dag {final_dag}'
            submit_dag_proc = subprocess.Popen(shlex.split(command), stdout = subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = submit_dag_proc.communicate()
            golum_logger_img2.info(f'{out}') 
            
        else:
            golum_logger_img2.info('To submit, use the following command: \n\t$ condor_submit_dag %s', final_dag)


if __name__ == "__main__":
    main()
