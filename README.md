# golum_pipe

Git repo to automatize GOLUM. This should rely on bilby_pipe, and make the ASIMOV integration easier. This branch is dedicated to O4 review.

This branch is dedicated to the development of `GOLUM` and `GOLUM_pipe` for O4. It should be capable of going from simple GOLUM runs with unlensed PE, to full joint PE and population reweighting for 4 images.

### Git Structure and general info about GOLUM

In essence, there are 2 different ways to run joint parameter estimation: a) you analyze the two data streams jointly, and b) you "distribute" the runs, analyzing first one image using an adapted waveform and then the second image using the posteriors provided by the second image. Option b) is significantly faster as the run time is dominated by the first image run, which is about the same a usual BBH PE run. However, in this case, we have found out that the choice of first and second image matters. Therefore, we implemented the possibility to run `symmetric GOLUM`, where we in parallel analyze the two images, once where it is the first image and one where it is the second image. This has been proven to be more precise, and since the two runs can be done in parallel, it is not increasing run times too much. Option b) also gives the advantage that it can search for type II images only, which us another signature for strong lensing based on a single image. 

Different examples can be fond in the `Example` folder. 