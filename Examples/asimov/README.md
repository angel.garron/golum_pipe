# Basic instruction to start an Asimov run

First, you need to use the correct asimov version, which momentarily corresponds to the one in [this branch](https://git.ligo.org/justin.janquart/asimov-lensingflow-dev/-/tree/lensingflow-infrastructure?ref_type=heads).

Then, you need to set the ini, prior and yaml files in a repo. Then, you also need to adapt path in the files to match your own repos. 

After that, do the following steps:
- `asimov init NAME_of_test` to create the asimov project in the folder. 
- `asimov apply -f event.yaml` to read in the information about the events you want to use.
- `asimov apply -f analysis.yaml` to read in the information about the analysis you want to do. Note that for the single image analysis, asimov will ask you which event you want to apply things on. In these examples, events are `S0` or `S1`.
- In the `.asimov/asimov.conf` file, add 
```
[templating]
directory = .
```
- `asimov manage build` to construct the ini files that will be used for running 
- `asimov manage submit` to actually start the jobs on condor and get them running. 

If you want asimov to start automatically a cron job that will monitor the jobs, you can do `asimov start`. Once the runs are done, this will also do the necessary post-processing step automatically. If you want to start these steps manually, you can also do `asimog monitor --chain`, which will do the various steps like checking which runs are done or not, whether post-process steps are required, etc. 
